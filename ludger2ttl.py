from rdflib import URIRef, Literal
from rdflib.namespace import RDF, DCTERMS, XSD
from rdflib import Graph
from prefix import glycan, root_prefix, bibo,dc,dcterms,owl
from literature import read_literature, read_papers
import uuid
import sys
import csv
import timeit
import math
start = timeit.default_timer()
maxInt = sys.maxsize
decrement = True

while decrement:
    # decrease the maxInt value by factor 10
    # as long as the OverflowError occurs.
    decrement = False
    try:
        csv.field_size_limit(maxInt)
    except OverflowError:
        maxInt = int(maxInt/10)
        decrement = True

def check_taxon(species): # Ludger has multiple taxon for one evidence entry
    list = []
    taxon = species.lower()
    if 'human' in taxon:
        list.append('Homo sapiens'.upper())
    if 'rabbit' in taxon:
        list.append('Oryctolagus cuniculus'.upper())
    if 'dog' in taxon:
        list.append('Canis lupus familiaris'.upper())
    if 'bovine' in taxon:
        list.append('Bos taurus'.upper())
    return list

def generate_uuid():
    uid_str_full = uuid.uuid4().urn
    uid_str = uid_str_full[9:]
    return uid_str

def ludger2ttl(csvfile,sample_id):

    # ----------------------  start to construct a graph for input data -------------------
    g = Graph()
    g.bind('glycan',glycan)
    g.bind('bibo',bibo)
    g.bind('dcterms',dcterms)
    g.bind('dc',dc)
    g.bind('owl', owl)


    # construct literature graph from literature files
    g = read_papers('/home/sophie/Dropbox/MQU/pubmed_result.csv',g)

    with open(csvfile, 'rb') as csvfile:
        next(csvfile) # skip the header in the first row
        infile = csv.reader(csvfile, delimiter='\t', quotechar='|')
        i = 1
        reference_prev = 'WG-50314'
        for row in infile:
            print '\n start ' + str(i) + " row\n"

            taxonomy = row[0]               # may be empty
            bio_source = row[1]             # full record
            cell_type = row[2]              # may be empty
            is_recombinant = row[3]         # may be empty
            glycan_class = row[4]           # full record
            release_method = row[5]         # may be empty
            instrument = row[6]             # full record
            column = row[7]                 # full record
            technique = row[8]              # full record
            uoxf = row[9]                   # full record
            glycobase_id = row[10]          # full record
            label = row[11]                 # full record
            rel_abundance = row[12]         # may be empty
            gu = row[13]                    # full record
            mono_mass = row[14]             # may be empty, only >30000 will have this value
            observed_mass_wlabel = row[16]  # may be empty
            is_ms1_confirmed = row[17]      # may be empty
            is_ms2_confirmed = row[18]      # may be empty
            is_digestion_confirmed = row[19]    # full record
            reference = row[21].replace(" ","")             # may be empty
            date = row[22]                  # may be empty
            operator = row[23]              # full record

            print uoxf
            if reference != reference_prev:
                sample_id += 1

            reference_prev = reference
            structure_URI = URIRef(root_prefix.structure + '/' + glycobase_id)

            # start a statement for structure class
            g.add((structure_URI, RDF.type, glycan.saccharide))
            g.add((structure_URI, glycan.has_uoxf, Literal(uoxf)))
            g.add((structure_URI, glycan.has_glycobase_id,Literal(glycobase_id,datatype=XSD.integer)))
            if glycan_class.upper() == 'N':
                g.add((structure_URI,RDF.type, glycan.N_glycan))
            elif glycan_class.upper() == 'O':
                g.add((structure_URI, RDF.type, glycan.O_glycan))
            if mono_mass:
                g.add((structure_URI, glycan.has_monoisotopic_molecular_weight, Literal(float(mono_mass),datatype=XSD.float)))

            # add resource_entry
            resource_URI = URIRef(root_prefix + glycobase_id)
            g.add((structure_URI, glycan.has_resource_entry, resource_URI))
            g.add((resource_URI, RDF.type, glycan.resource_entry))
            g.add((resource_URI, DCTERMS.identifier,Literal(glycobase_id)))
            g.add((resource_URI, glycan.in_glycan_database, glycan.database_glycostore))

            # add reference_compound
            evidence_identifier = str(glycobase_id) + '-' + str(reference)
            ref_comp_URI = URIRef(root_prefix.referencecompound + '/' + evidence_identifier)
            g.add((ref_comp_URI, RDF.type, glycan.referenced_compound))
            g.add((ref_comp_URI, glycan.has_glycan, structure_URI))

            lc_peak_URI = URIRef(root_prefix.lc_peak + '/' + evidence_identifier)
            if technique.upper() == 'UPLC':
                profile_URI = URIRef(root_prefix.uplc + '/' + str(reference))
                g.add((profile_URI, RDF.type, glycan.evidence_uplc))
            elif technique.upper() == 'HPLC':
                profile_URI = URIRef(root_prefix.hplc + '/' + str(reference))
                g.add((profile_URI, RDF.type, glycan.evidence_hplc))

            g.add((ref_comp_URI, glycan.has_lc_chromatogram_peak, lc_peak_URI))
            g.add((structure_URI, glycan.has_lc_chromatogram_peak, lc_peak_URI))
            g.add((profile_URI, glycan.has_lc_chromatogram_peak, lc_peak_URI))

            g.add((ref_comp_URI,glycan.is_from_profile, profile_URI))
            g.add((ref_comp_URI, glycan.is_from_report, Literal('Ludger')))

            g.add((profile_URI, glycan.used_column, Literal(column)))
            g.add((profile_URI, glycan.used_instrument, Literal(instrument)))
            g.add((profile_URI, glycan.has_label, Literal(label)))
            if release_method:
                g.add((profile_URI, glycan.release_method, Literal(release_method)))
            if date:
                g.add((profile_URI, dc.date, Literal(date,datatype=XSD.date)))
            if is_digestion_confirmed:
                g.add((ref_comp_URI, glycan.is_exoglycosidase_confirmed, Literal('yes')))
            g.add((profile_URI, dc.operator, Literal(operator)))
            g.add((profile_URI, glycan.contributed_by, Literal('Ludger')))
            g.add((lc_peak_URI, glycan.has_glucose_unit, Literal(gu,datatype=XSD.float)))
            if rel_abundance.isdigit():
                g.add((lc_peak_URI, glycan.has_peak_area, Literal(rel_abundance)))

            # ------------------- source URI -------------------------
            # arbitrary sample id, follow bti, gsl, start from 270 for sample
            source_URI = URIRef(root_prefix.source + '/' + str(sample_id))
            g.add((ref_comp_URI, glycan.is_from_source, source_URI))
            g.add((source_URI, RDF.type, glycan.source_nature))
            g.add((source_URI, glycan.from_sample, Literal(bio_source)))
            g.add((source_URI, glycan.has_sample_type, Literal(bio_source)))
            if taxonomy:
                taxon_list = check_taxon(taxonomy)
                for item in taxon_list:
                    g.add((source_URI, glycan.has_taxon, Literal(item)))

            if is_recombinant:
                if is_recombinant.lower() == 'yes':
                    g.add((source_URI, glycan.hosted_by, Literal(cell_type)))

            # -------------------- evidence_ms ------------------------
            ref_ms_URI = URIRef(root_prefix.evidence_ms + '/'+ evidence_identifier)
            g.add((ref_comp_URI, glycan.has_evidence,ref_ms_URI))
            if is_ms1_confirmed: # if mass confirmed
                g.add((ref_ms_URI, glycan.ms1_verified, Literal('yes')))
                g.add((ref_ms_URI, glycan.observed_mass_with_label, Literal(observed_mass_wlabel,datatype=XSD.float)))
                if is_ms2_confirmed:
                    if is_ms2_confirmed.lower() == 'yes':
                        g.add((ref_ms_URI, glycan.ms2_verified, Literal('yes')))
            else:
                g.add((ref_ms_URI, glycan.ms1_verified, Literal('no')))

            # -------------- add literature reference------------------
            if reference.startswith(("WG","CAB","CPROC")):
                g.add((ref_comp_URI, glycan.has_reference, Literal(reference)))
            else:
                literature_URI = URIRef(root_prefix.literature + '/' + reference)
                g.add((ref_comp_URI, glycan.published_in, literature_URI))


            i+=1
        print 'current sample id:'
        print sample_id
        print '\nprocessed ' + str(i-1) + ' rows\n'
        stop = timeit.default_timer()
        time_s = stop-start
        time = math.modf(time_s/60)
        print '\n----------------------\nused ' + str(time[1]) + ' min ' + str(time[0]) +' s\n------------------------\n'

        # print g.serialize(format = 'turtle')
        return g.serialize(format = 'turtle')

full_file = '/home/sophie/Dropbox/workfile/glycostore/ludger-data-spread.csv'

tofile = '/home/sophie/Dropbox/workfile/glycostore/ludger201801.ttl'
f = open(tofile,'w')
f.write(ludger2ttl(full_file,270))
f.close()

## next sample_id should start after 346


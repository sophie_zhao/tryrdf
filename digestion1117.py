from rdflib import URIRef, Literal
from rdflib.namespace import RDF, DCTERMS, XSD
from prefix import glycan, root_prefix,resource_prefix
import uuid

def is_not_empty_array(str):
    if str[1:len(str)-1]:
        return True
    else:
        return False

def remove_last_comma(str):
    new_str = str[0:len(str)-1]
    return new_str

def extract_digestion_entry(item):
    cformat = item.replace("Technique:","|").replace("name:","|").replace("enzymes:","|").\
                            replace("retention (GU): ","|").replace("profile ID: ","|").replace("profile name:","|").\
                            replace("profile instrument: ","|").replace("profile dextran standard: ","|")
    list = cformat.split("|")
    return list

def split_digestion_entry(str):
    list = str[2:len(str)-3] #remove 3 from the back because the digestion entry ends with '],]'
    list_of_digest = list.split("],[")
    return list_of_digest

def generate_uuid():
    uid_str_full = uuid.uuid4().urn
    uid_str = uid_str_full[9:]
    return uid_str

def check_digest_evidence(type_str,profile_id):
    L = []
    if type_str == "UPLC":
        result_URI = URIRef(root_prefix.uplc + '/' + profile_id)
        type = glycan.evidence_uplc
        L = [result_URI,type]
    elif type_str == 'HPLC':
        result_URI = URIRef(root_prefix.hplc + '/' + profile_id)
        type = glycan.evidence_hplc
        L = [result_URI,type]
    elif type_str == 'CE':
        result_URI = URIRef(root_prefix.ce + '/' + profile_id)
        type = glycan.evidence_ce
        L = [result_URI,type]
    elif type_str == 'RPUPLC':
        result_URI = URIRef(root_prefix.rpuplc + '/' + profile_id)
        type = glycan.evidence_rpuplc
        L = [result_URI,type]
    return L

def check_reaction_enzyme(enzyme_str):
    if enzyme_str in ('ABS','ABS;ABS','ABS, ABS'):
        return 'ABS'
    elif enzyme_str in ('ABS;NANI','ABS, NANI','NANI;ABS','NANI, ABS'):
        return 'ABS-NANI'
    elif enzyme_str.upper() in ('BTG;SPG','BTG, SPG','SPG;BTG','SPG, BTG'):
        return 'BTG-SPG'
    elif enzyme_str in ('GUH;JBH','GUH, JBH','JBH;GUH','JBH, GUH'):
        return 'GUH-JBH'
    elif enzyme_str in ('GUH;JBH;SPH','GUH, JBH, SPH','SPH;JBH;GUH','SPH, JBH, GUH'):
        return 'GUH-JBH-SPH'
    else:
        return enzyme_str.upper()

def process_digest(rowlist,g,digestType,root_structure_id):
    root_structure_URI = URIRef(root_prefix.structure + '/' + root_structure_id)
    evidence_type = ''
    uoxf = ''
    for item in split_digestion_entry(rowlist):
        array_in_item = extract_digestion_entry(item)

        digest_id = remove_last_comma(array_in_item[0]) # glycobase id for digest child/parent
        StructureID = Literal(digest_id)
        if len(array_in_item)>1:
            evidence_type = remove_last_comma(array_in_item[1])
        if len(array_in_item)>2:
            uoxf = remove_last_comma(array_in_item[2])
            UOXF = Literal(uoxf)
        if len(array_in_item) > 3:
            enzyme = remove_last_comma(array_in_item[3])
            if len(array_in_item) > 4:
                gu = remove_last_comma(array_in_item[4])
                GU = Literal(gu,datatype=XSD.float)
                if len(array_in_item) > 5:
                    profile_id = remove_last_comma(array_in_item[5]).split(",")[0]

                    if profile_id == 'NA':
                        profile_id = generate_uuid()
                    # check if digest child/parent is current row structure, add following only if not same structure
                    if root_structure_id != str(digest_id):
                        if digestType == 'child':
                            reaction_id = root_structure_id+'-'+str(digest_id)+'/'+check_reaction_enzyme(enzyme.strip())
                        else:
                            reaction_id = str(digest_id)+'-'+root_structure_id+'/' + check_reaction_enzyme(enzyme.strip())
                        digest_URI = URIRef(root_prefix.digest + '/'+ reaction_id)    ##
                        g.add((digest_URI, glycan.has_exglycosidase_treatment, Literal(check_reaction_enzyme(enzyme.strip()))))

                        ref_comp_URI = URIRef(root_prefix.referencecompound + '/' + digest_id + '-' + profile_id)
                        structure_URI = URIRef(root_prefix.structure + '/' + str(digest_id))
                        g.add((ref_comp_URI, RDF.type, glycan.referenced_compound))
                        g.add((ref_comp_URI, glycan.has_glycan, structure_URI))
                        g.add((structure_URI, RDF.type, glycan.saccharide))
                        evidence_URI = check_digest_evidence(evidence_type,profile_id)[0]
                        checked_type = check_digest_evidence(evidence_type,profile_id)[1]
                        g.add((evidence_URI, RDF.type, checked_type))

                        if evidence_type == 'CE':
                            g.add((evidence_URI,glycan.has_ce_digest_chromatogram,digest_URI))
                            ce_peak_URI = URIRef(root_prefix.ce_peak + '/' + digest_id+'-'+profile_id)
                            g.add((ref_comp_URI, glycan.has_ce_peak, ce_peak_URI))
                            g.add((evidence_URI, glycan.has_ce_peak, ce_peak_URI))
                            g.add((structure_URI, glycan.has_ce_peak, ce_peak_URI))
                            g.add((ce_peak_URI, glycan.has_glucose_unit, GU))
                        else:
                            g.add((evidence_URI, glycan.has_lc_digest_chromatogram, digest_URI))
                            lc_peak_URI = URIRef(root_prefix.lc_peak + '/' + digest_id + '-' + profile_id)
                            g.add((ref_comp_URI, glycan.has_lc_chromatogram_peak, lc_peak_URI))
                            g.add((evidence_URI, glycan.has_lc_chromatogram_peak, lc_peak_URI))
                            g.add((structure_URI, glycan.has_lc_chromatogram_peak, lc_peak_URI))
                            if evidence_type == 'RPUPLC':
                                g.add((lc_peak_URI,glycan.has_arabinose_unit,GU))
                            else:
                                g.add((lc_peak_URI, glycan.has_glucose_unit, GU))

                        if len(array_in_item) > 7:
                            instrument = remove_last_comma(array_in_item[7]).strip()
                            if instrument != 'NA':
                                Instrument = Literal(instrument)
                                g.add((digest_URI, glycan.used_instrument,Instrument))
                        if len(array_in_item) > 8:
                            pf_dextran_sd = array_in_item[8].strip()
                            if pf_dextran_sd != "NA":
                                PrflDextran = Literal(pf_dextran_sd)
                                g.add((digest_URI, glycan.dextran_standard, PrflDextran))



                        reaction_URI = URIRef(root_prefix.reaction + '/' + reaction_id)
                        g.add((structure_URI, glycan.has_reaction, reaction_URI))
                        g.add((reaction_URI, glycan.has_exglycosidase, Literal(check_reaction_enzyme(enzyme.strip()))))
                        if digestType == 'child':
                            g.add((reaction_URI, glycan.has_substrate, root_structure_URI))
                            g.add((reaction_URI, glycan.has_product, structure_URI))
                        else:
                            g.add((reaction_URI, glycan.has_substrate, structure_URI))
                            g.add((reaction_URI, glycan.has_product, root_structure_URI))

                        resource_URI = URIRef(resource_prefix + str(digest_id))
                        g.add((structure_URI, glycan.has_resource_entry, resource_URI))
                        g.add((resource_URI, RDF.type, glycan.resource_entry))
                        g.add((resource_URI, glycan.in_glycan_database, glycan.database_glycobase))
                        g.add((resource_URI, DCTERMS.identifier,StructureID))
                        g.add((structure_URI, glycan.has_uoxf, UOXF))

    return g

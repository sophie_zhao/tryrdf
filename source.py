from rdflib import URIRef, Literal
from rdflib.namespace import RDF
from prefix import glycan, root_prefix


def add_source(graph,sample_title,sample_id,ref_comp_URI):
    taxon = ''
    tissue =''
    if sample_title == 'Cow':
        taxon = 'Bos taurus'.upper()
        tissue = 'Anatomy'
    elif sample_title in ('Human Serum','Human Serum'):
        taxon = 'Homo sapiens'.upper()
        tissue = 'Anatomy'
    elif sample_title == 'Pig':
        taxon = 'Sus scrofa'.upper()
        tissue = 'Milk'
    elif sample_title == 'Sheep':
        taxon = 'Ovis aries'.upper()
        tissue = 'Anatomy'
    elif sample_title == 'Horse':
        taxon = 'Equus caballus'.upper()
        tissue = 'Milk'
    elif sample_title == 'Goat':
        taxon = 'Capra aegagrus hircus'.upper()
        tissue = 'Milk'
    elif sample_title == 'Dromedary Camel':
        taxon = 'Camelus dromedarius'.upper()
        tissue = 'Milk'
    elif sample_title in ('CHO B', 'CHO A'):
        taxon = 'Cricetulus griseus'.upper()
        tissue = 'Anatomy'
    else:
        taxon = False
        tissue = False

    source_URI = URIRef(root_prefix.source + '/' + sample_id)
    graph.add((ref_comp_URI, glycan.is_from_source, source_URI))
    graph.add((source_URI, RDF.type, glycan.source_nature))
    graph.add((source_URI, glycan.from_sample, Literal(sample_title)))
    if taxon and tissue:
        graph.add((source_URI, glycan.has_taxon, Literal(taxon)))
        graph.add((source_URI, glycan.has_tissue, Literal(tissue)))
    return graph

def add_source_protein(graph, sample_title, sample_id, structure_URI):
    protein_URI = URIRef(root_prefix.proteinsummary + '/'+ sample_id)
    if sample_title in ('Haptoglobin','Human IgG','IgG','RNAse B','Transferrin'):
        graph.add((protein_URI, glycan.has_protein_name, Literal(sample_title)))
        graph.add((protein_URI, glycan.has_attached_glycan, structure_URI))
    return graph


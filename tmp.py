import sys, os
import glob
import csv
from unicornRestMass import matchGlycoCT


# picture_dir = '/home/sophie/Dropbox/database_dev/GSL-glycan-lib'
# name_reference_dir = '/home/sophie/Dropbox/workfile/glycostore'
# gsl_name_id_file = 'yati-gsl-name-id.txt'
# current, dirs, files = os.walk(picture_dir).next()
#
#
# with open(os.path.join(name_reference_dir,gsl_name_id_file), 'rb') as csvfile:
#     '''
#     one series of GSL is deemed as one profile, series name go to report name; one sub category inside series is
#     viewed as one sample, category name go to sample name;
#     sample_prep id 1 for 2ab; 2 for procainamide.
#     '''
#     next(csvfile) # skip the header in the first row
#     data_file = csv.reader(csvfile, delimiter='\t', quotechar='|')
#
#     for row in data_file:
#         code = row[0]
#         glycobase_id = row[2]
#
#         for dir in dirs:
#             if dir.startswith('lg-'):
#                 current_abs_dir = os.path.join(picture_dir,dir)
#                 # print current_abs_dir
#                 for file in glob.glob(os.path.join(current_abs_dir,'lg-*.png')):
#                     if code in file:
#                         new_name = file.replace(code, glycobase_id)
#                         os.rename(file,new_name)

def extract_uniprot(csvfile):
    f = open('/home/sophie/Documents/uniprot/kat_uniprot_final_extract.txt', 'a')
    with open(csvfile, 'rb') as csvfile:
        infile = csv.reader(csvfile, delimiter='\t', quotechar='|')

        uniprot_id = ''
        protein_name =''
        gene_name = ''
        taxonomy = ''
        key_word =''
        location = ''
        function = ''
        glycosylation_site=''
        is_glycoprotein = ''
        function_in_read = False
        location_in_read = False
        i=1
        j = 0
        f.write("uniprot_id\tprotein_name\tgene_name\ttaxonomy\tkey_word\tis_glycoprotein\tlocation\tfunction\tglycosylation_site")
        for row in infile:


            line_array = row[0].split("   ")
            tag = line_array[0]
            content = line_array[1]

            if tag == "AC":

                print "reading AC: " + content
                if j == 0:
                    uniprot_id = content.split(";")[0]
                    # mark already read one AC
                    j += 1

            elif tag == "DE":
                print "under DE: " + content
                # add protein name to the second field of the output line
                if content.split(":")[0] == "RecName":
                    # remove ";" from the name
                    protein_name = content.split("=")[1].split(";")[0]
            elif tag == "GN":
                if "Name=" in content:
                    # add gene name to output
                    gene_name = content.split(";")[0].split("=")[1]
            elif tag == "OS":
                # add taxonomy info
                taxonomy = content.split(".")[0]
            elif tag == "RP":
                # add annotation for site if glycosylated
                rp_head = content.split(" ")[0]
                if rp_head == 'GLYCOSYLATION':
                    glycosylation_site += content
            elif tag == "CC":

                if "-!-" in content:
                    # print "reading CC new section: "
                    function_in_read = False
                    location_in_read = False

                    if "FUNCTION" in content:
                        function = content.split("FUNCTION:")[1]
                        function_in_read = True
                    elif "SUBCELLULAR LOCATION" in content:
                        location = content.split("LOCATION:")[1]
                        location_in_read = True
                elif function_in_read:
                    function += line_array[2]
                elif location_in_read:
                    location += line_array[2]
            elif tag == "KW":
                key_word += content
                if "Glycoprotein" in content:
                    is_glycoprotein = 'Yes'
            elif tag == "SQ":
                f.write("\n"+uniprot_id+'\t'+protein_name+'\t'+gene_name+'\t'+taxonomy+'\t'+key_word+'\t'+is_glycoprotein+'\t'
                            +location + '\t' + function+'\t' + glycosylation_site)
                j = 0
                uniprot_id = ''
                protein_name =''
                gene_name = ''
                taxonomy = ''
                key_word =''
                location = ''
                function = ''
                glycosylation_site=''
                is_glycoprotein = ''
                function_in_read = False
                location_in_read = False

            i+=1
        # write the last one


    f.close()





# full_file = '/home/sophie/Documents/uniprot/kat_uni_lost_resource.txt'
# extract_uniprot(full_file)

infile="/home/sophie/Downloads/id-no-mass.txt"
def column2array(csvfile):
    out=[]
    with open(csvfile, 'rb') as csvfile:
        infile = csv.reader(csvfile, delimiter='\t', quotechar='|')
        for row in infile:
            out.append(row[0])
    return out

print column2array(infile)

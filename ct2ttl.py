from rdflib import URIRef, Literal
from rdflib.namespace import RDF, DCTERMS, XSD
from rdflib import Graph
from prefix import glycan, root_prefix,dc,dcterms
import sys
import csv
maxInt = sys.maxsize
decrement = True


while decrement:
    # decrease the maxInt value by factor 10
    # as long as the OverflowError occurs.
    decrement = False
    try:
        csv.field_size_limit(maxInt)
    except OverflowError:
        maxInt = int(maxInt/10)
        decrement = True

def read_file(infile):
    with open(infile, 'r') as f:
        data = [row for row in csv.reader(f.read().splitlines())]
    return data

def ct2ttl(infile):
    '''
        structure URI are existed, just need to generate a small patch recording glycoCT
    '''

    # ----------------------  start to construct a graph for input data -------------------
    g = Graph()
    g.bind('glycan',glycan)
    g.bind('dcterms',dcterms)
    g.bind('dc',dc)

    # data=read_file(infile)
    # for row in data:
    with open(infile, 'rU') as csvfile:
        csvreader = csv.reader(csvfile)
        for row in csvreader:
            print ', '.join(row)
            glycobase_id = row[0]
            ct_content = row[1]
            # print ct_content
            structure_URI = URIRef(root_prefix.structure + '/' + str(glycobase_id))
            ct_sequence_URI = URIRef(root_prefix.structure + '/' + glycobase_id + "/ct")        # fit unicarbkb
            g.add((structure_URI, glycan.glycosequence, ct_sequence_URI))
            g.add((structure_URI, glycan.has_glycosequence, ct_sequence_URI))
            ct_sequence = Literal(ct_content)
            g.add((ct_sequence_URI, RDF.type, glycan.glycosequence))
            g.add((ct_sequence_URI, glycan.has_sequence, ct_sequence))
            g.add((ct_sequence_URI, glycan.in_carbohydrate_format, glycan.carbohydrate_format_glycoct))

    return g.serialize(format = 'turtle')


ct_in = "/home/sophie/Dropbox/workfile/glycostore/nonGlycoBase_glycoCT2.csv"
ct_out = "/home/sophie/Dropbox/workfile/glycostore/glycoCTforNonGlycoBase2.ttl"
f = open(ct_out,'w')
f.write(ct2ttl(ct_in))
f.close()

# ct2ttl(ct_in)


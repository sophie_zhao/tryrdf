from rdflib import URIRef, Literal
from rdflib.namespace import RDF, DCTERMS, XSD
from rdflib import Graph
from prefix import glycan, root_prefix,dc,dcterms
from literature import read_papers
import sys
import csv
maxInt = sys.maxsize
decrement = True


while decrement:
    # decrease the maxInt value by factor 10
    # as long as the OverflowError occurs.
    decrement = False
    try:
        csv.field_size_limit(maxInt)
    except OverflowError:
        maxInt = int(maxInt/10)
        decrement = True

def gslTTL(gsl_file,profile_id,sample_id):

    # ----------------------  start to construct a graph for input data -------------------
    g = Graph()
    g.bind('glycan',glycan)
    g.bind('dcterms',dcterms)
    g.bind('dc',dc)
    # read_papers(paper_record_csv,g)

    with open(gsl_file, 'rb') as csvfile:
        '''
        one series of GSL is deemed as one profile, series name go to report name; one sub category inside series is
        viewed as one sample, category name go to sample name;
        sample_prep id 1 for 2ab; 2 for procainamide.
        '''
        next(csvfile) # skip the header in the first row
        data_file = csv.reader(csvfile, delimiter='\t', quotechar='|') #NIBRT
        # data_file = csv.reader(csvfile, delimiter=',', quotechar='|') #BTI

        sample = 'standards (Sigma-Aldrich)'
        for row in data_file:
            # code = row[1] # BTI commercial reference, as extra info
            glycobase_id = row[1] # NIBRT
            # glycobase_id = row[2] # BTI
            # name = row[3]   # BTI common names for GSL, show in round bracket under composition/UOXF
            composition = row[2] # NIBRT composition- equivalent to uoxf in glycobase
            # composition = row[4] #BTI
            GU2ab = row[3]  # NIBRT observed GU
            # GU2ab = row[10] #BTI
            # GUproc = row[18]  # expected mass with label
            try:
                MW = row[4]  # NIBRT monoisotopic mass without label
                # MW = row[7] #BTI
            except:
                MW = False
            series = row[5] # NIBRT GSL series including globo, ganglio, neolacto etc.
            # series = row[15] # BTI
            # category = row[16]  # category including isoglobo series, blood group antigen etc.
            # category_num = row[17]
            source = row[6]     #NIBRT
            # reference = row[7]  #NIBRT

            # if GUproc and GU2ab:
            # # above if is for gsl yati file only

                # # define URIs using ID, glycobase_id below 30000 is from GlycoBase, above is new structure
                # # glycobase id is an id used for glycostore internally only, make sure it's unique for each structure
            structure_URI = URIRef(root_prefix.structure + '/' + str(glycobase_id))
            # start a statement for structure class
            g.add((structure_URI, RDF.type, glycan.glycoconjugate))
            g.add((structure_URI, RDF.type, glycan.glycolipid))
            g.add((structure_URI, glycan.has_uoxf, Literal(composition)))
            # g.add((structure_URI, glycan.has_code, Literal(code)))
            g.add((structure_URI, glycan.has_series, Literal(series)))
            # g.add((structure_URI, glycan.has_alternative_name, Literal(name)))
            if MW:
                g.add((structure_URI, glycan.has_monoisotopic_molecular_weight, Literal(MW,datatype= XSD.float)))
            g.add((structure_URI, glycan.has_glycobase_id,Literal(glycobase_id,datatype=XSD.integer)))

            ## generate reference compound
            def ref_comp(profile_id,gu,label,sample_id):
                # one profile for 2AB labelled standard, one for procainamide labelled
                # two profiles in total, no need to auto increase
                evidence_identifier = str(glycobase_id) + '-' + str(profile_id)
                ref_comp_URI = URIRef(root_prefix.referencecompound + '/' + evidence_identifier)
                g.add((ref_comp_URI, RDF.type, glycan.referenced_compound))
                g.add((ref_comp_URI, glycan.has_glycan, structure_URI))
                source_URI = URIRef(root_prefix.source + '/' + str(sample_id))
                g.add((ref_comp_URI, glycan.is_from_source, source_URI))
                g.add((source_URI, RDF.type, glycan.source_nature))
                # g.add((source_URI, glycan.from_sample, Literal("GSL standards"))) #BTI
                g.add((source_URI, glycan.from_sample, Literal(source))) #NIBRT

                # add resource_entry
                resource_URI = URIRef(root_prefix + glycobase_id)
                g.add((structure_URI, glycan.has_resource_entry, resource_URI))
                g.add((resource_URI, RDF.type, glycan.resource_entry))
                g.add((resource_URI, DCTERMS.identifier,Literal(glycobase_id)))
                g.add((resource_URI, glycan.in_glycan_database, glycan.database_glycostore))

                # # ------------------- source URI ------BTI-------------------
                # if category_num:
                #     # category is an attribute to structure rather than ref_comp
                #
                #     g.add((structure_URI, glycan.is_from_category, Literal(category)))

                # # -------------- add literature reference-----------NIBRT-------
                # literature_URI = URIRef(root_prefix.literature + '/' + reference)
                # g.add((ref_comp_URI, glycan.published_in, literature_URI))

                # -------------------- evidence_ms ------------------------
                ref_ms_URI = URIRef(root_prefix.evidence_ms + '/'+ evidence_identifier)
                g.add((ref_comp_URI, glycan.has_evidence,ref_ms_URI))
                g.add((ref_ms_URI, glycan.ms1_verified, Literal('yes'))) # all entries with GU value are ms1 verified
                # ------------------- lc_chromatogram_peak ------------------
                lc_peak_URI = URIRef(root_prefix.lc_peak + '/' + evidence_identifier)
                g.add((ref_comp_URI, glycan.has_lc_chromatogram_peak, lc_peak_URI))
                g.add((ref_comp_URI, glycan.is_from_report, Literal('NIBRT GSL')))
                # g.add((ref_comp_URI, glycan.is_from_report, Literal('BTI GSL Standards')))
                g.add((structure_URI, glycan.has_lc_chromatogram_peak, lc_peak_URI))
                evidence_uplc_URI = URIRef(root_prefix.uplc + '/' + str(profile_id))
                g.add((ref_comp_URI,glycan.is_from_profile, evidence_uplc_URI))
                g.add((evidence_uplc_URI, RDF.type, glycan.evidence_uplc))
                g.add((evidence_uplc_URI, glycan.has_lc_chromatogram_peak, lc_peak_URI))
                g.add((evidence_uplc_URI, glycan.used_column, Literal('Waters BEH Glycan')))
                g.add((evidence_uplc_URI, glycan.used_instrument, Literal('Waters HClass')))
                g.add((evidence_uplc_URI, glycan.contributed_by, Literal('NIBRT')))
                # g.add((evidence_uplc_URI, glycan.used_column, Literal('ACQUITY UPLC BEH Glycan')))
                # g.add((evidence_uplc_URI, glycan.used_instrument, Literal('UNIFI 1.7')))
                # g.add((evidence_uplc_URI, glycan.contributed_by, Literal('BTI-Analytics')))

                g.add((evidence_uplc_URI, glycan.used_standard, Literal('Dextran ladder')))
                g.add((evidence_uplc_URI, glycan.has_label, Literal(label)))
                g.add((lc_peak_URI, glycan.has_glucose_unit, Literal(gu,datatype=XSD.float)))

                # -------------------- creation date --------------------------
                g.add((ref_comp_URI, dc.date, Literal('2016-04-01',datatype=XSD.date)))
                # g.add((ref_comp_URI, dc.date, Literal('2016-01-10',datatype=XSD.date)))
                # -------------------- operator ------------------------------
                g.add((ref_comp_URI, dc.operator, Literal('Simone Albrecht')))
                # g.add((ref_comp_URI, dc.operator, Literal('Noor Hayati Bte Kamari')))

            if source != sample:
                sample = source
                sample_id += 1
                profile_id += 1

            ref_comp(profile_id,GU2ab,'2-AB',sample_id)
            # ref_comp(profile_id+100,GUproc,'Procainamide',sample_id)
            # # finished reading current line
        print profile_id
        print sample_id


    # print g.serialize(format = 'turtle')
    # print glycobase_id
    # print profile_id
    # print sample_id
    return g.serialize(format = 'turtle')

# profileID = 2160 # BTI
# sampleID = 260     # BTI
profileID = 2161 # NIBRT
sampleID = 261  # NIBRT
# new profile id for gsl yati: 2160, sample id: 260
# 2163 profile Id, 263 sample id used for nibrt gsl, 2016-04-01

# literature_csv = '/home/sophie/Downloads/pubmed_result.csv'
nibrt_gsl = "/home/sophie/Dropbox/workfile/glycostore/gsl-nibrt-in-use.csv"
nibrt_out = "/home/sophie/Dropbox/workfile/glycostore/gsl-nibrt-2017.ttl"

# yati_gsl = '/home/sophie/Dropbox/database_dev/GSL-glycan-lib/glycolipid-sum.csv'
# tofile = '/home/sophie/Dropbox/workfile/glycostore/gsl-yati-2017.ttl'

# f = open(tofile,'w')
f = open(nibrt_out,'w')
# f.write(gslTTL(yati_gsl,profileID,sampleID))
f.write(gslTTL(nibrt_gsl,profileID,sampleID))
f.close()


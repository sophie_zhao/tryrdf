# -*- coding: utf-8 -*-
import csv
import string
import glob, os

# tofile = '/home/sophie/Desktop/dataWithID.txt'
# rawExpFile = '/home/sophie/Desktop/chen-experiment-data.txt'

def findID(rawExpFile,tofile,btiID):
    idLib = '/home/sophie/Dropbox/MQU/glycobase-name-id.txt'
    # btiID = 30001  # new glycans not present in current glycobase will use btiID, start from 30001

    file = open(tofile,'w')
    with open(rawExpFile, 'rb') as expfile:
        # next(expfile)
        rawData = csv.reader(expfile, delimiter='\t')
        # file.write('name\tid\tamount%\texpectedGU\tobservedGU\texpectedMass\tobserved_m/z\tcharge\n')
        file.write('name\tid\n')
        for line in rawData:
            found = 0
            # # ---------------for BTI unifi data processed in March 2016 -----------
            # # UNIFI output give unusual space character, need to be replaced with common ones to be recognized.
            # name = string.replace(line[0],' ',' ')
            # # ---------------------------------------------------------------------
            print line[0]
            name = line[0]

            with open(idLib, 'rb') as csvfile:
                next(csvfile)
                libfile = csv.reader(csvfile, delimiter='\t')
                for row in libfile:
                # print 'lib row: ' + row[0]

                    if row[0]== name:
                        found = 1
                        # name-glycobaseID-amount%-expectedGU-observedGU-expected mass (with label) -observed m/z (with label)-charge
                        # file.write(name + "\t" + row[1] + "\t" + line[3] + "\t" + line[4] + "\t" + line[5] + "\t"
                        #            + line[6] + "\t" + line[7] + "\t" + line[8] +'\n')
                        file.write(name+"\t"+row[1]+'\n')
            # if found == 0:
            #     # file.write(name + "\t" + str(btiID) + "\t" + line[3] + "\t" + line[4] + "\t" + line[5] + "\t"
            #     #             + line[6] + "\t" + line[7] + "\t" + line[8] +'\n')
            #     file.write(name+"\t"+str(btiID)+"\n")
            #     with open(idLib,'a') as libF:
            #         libF.write( name+'\t'+str(btiID) + '\n')
            #     btiID += 1
    file.close()

# os.chdir("/home/sophie/Dropbox/workfile/bti2ttl/ChenChen_human_serum")
# # Chen Chen file, YS_LTC batch 10 files, YS_DG44 batch 8 files, WL_HER 12 files,
# # YS_marker 10 files, cell_culture batch 7 files didn't find new structures
# # btiID = 30001 # used for susanto AAT batch 4 files
btiID = 30300
# for file in glob.glob("data_*.txt"):
#     outputF = "id_" + file
#     findID(file,outputF,btiID)
#     btiID += 30
# print btiID
# in_file = ("/home/sophie/Dropbox/workfile/glycostore/gsl-nibrt-in-use.csv")
# # yati_gsl = '/home/sophie/Dropbox/database_dev/GSL-glycan-lib/glycolipid-sum.csv'
# out_file = ("/home/sophie/Dropbox/workfile/glycostore/gsl-nibrt-id.txt")
# findID(in_file,out_file,btiID)

directory_origin = '/home/sophie/Downloads/terry/'
destination_file = '/home/sophie/Desktop/terry_id.csv'
profileID = 2001
sampleID = 201

def find_id(parent_dir):
    current, dirs, files = os.walk(parent_dir).next()
    for file in glob.glob(os.path.join(directory_origin,'*.csv')):
        print 'file: ' +file
        tofile = file.split(".")[0]+'_id.txt'
        # execute the function
        findID(file,tofile,0)

find_id(directory_origin)




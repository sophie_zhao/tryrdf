from rdflib import URIRef, Literal
from rdflib.namespace import RDF
from prefix import root_prefix,dc,glycan,dcterms,bibo,owl,pubmed_prefix
import csv

def split_literature(str):
    list = str[3:len(str)-3] #remove 3 from the back because the digestion entry ends with '],]'
    list_of_reference = list.split("], [")
    return list_of_reference

def remove_last_comma(str):
    str_no_rspace = str.strip()
    new_str = str_no_rspace[0:len(str_no_rspace)-1]
    return new_str

def regulate_ms(msRecord):
    if msRecord:
        if msRecord[0] in ('y','Y'):
        #     result = 'yes'
        # elif msRecord[0] in ('d','D'):
        #     result = 'DS'
        # elif msRecord[0] == 'n':
        #     result = 'nd'
        # else:
        #     result = 'No'
            return "yes"
        else:
            return "no"
    else:
        return 'no'

def extract_literature_element(item):
    cformat = item.replace("reference_id: ","|").replace("author: ","|").replace("paper_gu: ","|").\
                            replace("paper_title: ","|").replace("paper_journal: ","|").replace("paper_issue: ","|").\
                            replace("paper_pages: ","|").replace("paper_date: ","|").replace("paper_pubmed: ","|").\
                            replace("MS/MS: ","|").replace("MS: ","|")
    list = cformat.split("|")
    return list

def read_literature(rowlist): # 28papers from 1997-2010, assume all comes from hplc data
    list_of_reference = split_literature(rowlist)
    gu_list = []
    ms2_list = []
    ms1_list = []
    pubmed_id_list = []
    for item in list_of_reference: # item is a literature
        array_in_item = extract_literature_element(item)
        length = len(array_in_item)
        if length == 11:
            pubmed_id = remove_last_comma(array_in_item[8])
            gu = remove_last_comma(array_in_item[2])
            ms2 = remove_last_comma(array_in_item[9])
            ms1 = array_in_item[10]
        else: # length = 12
            pubmed_id = remove_last_comma(array_in_item[9])
            gu = remove_last_comma(array_in_item[3])
            ms2 = remove_last_comma(array_in_item[10])
            ms1 = array_in_item[11]
        ms2_value = regulate_ms(ms2)
        ms1_value = regulate_ms(ms1)
        gu_list.append(gu)
        ms2_list.append(ms2_value)
        ms1_list.append(ms1_value)
        pubmed_id_list.append(pubmed_id)
    literature_content = [gu_list,ms2_list,ms1_list,pubmed_id_list]
    return literature_content

def read_papers(csvfile,g):

    with open(csvfile, 'rb') as csvfile:
        next(csvfile) # skip the header in the first row
        infile = csv.reader(csvfile, delimiter=',', quotechar='"')
        for row in infile:
            title = row[0]
            author_list_str = row[2]
            pubmed_id = row[1].split("/")[2] # type = string
            # if the literature is a research paper indexed in pubmed
            if pubmed_id.isdigit():
                literature_URI = URIRef(root_prefix.literature + '/' + pubmed_id)
                g.add((literature_URI, RDF.type, bibo.Article))
                g.add((literature_URI, glycan.has_pmid, Literal(pubmed_id)))
                details = row[3].split(";")[1] # poster doesn't have details
                volume = details.split("(")[0]
                page_start = details.split(":")[1].split("-")[0]
                page_end = details.split(":")[1].split("-")[1]
                g.add((literature_URI, bibo.volume, Literal(volume)))
                g.add((literature_URI, bibo.pageStart, Literal(page_start)))
                g.add((literature_URI, bibo.pageEnd, Literal(page_end)))
                journal = row[4].split(".")[0]
                year = row[4].split(".")[1]
                journal_id = row[11] # manually classified journals in excel and assigned each a journal id
                journal_fn = row[12] # manually added journal full name
                journal_URI = URIRef(root_prefix.journal+'/'+str(journal_id))
                g.add((literature_URI, dcterms.isPartOf, journal_URI))
                g.add((journal_URI, RDF.type, bibo.journal))
                g.add((journal_URI, dc.title, Literal(journal_fn)))
                g.add((journal_URI, bibo.shortTitle, Literal(journal)))
                resource_URI = URIRef(pubmed_prefix + pubmed_id)

            else:
                # if the literature is a poster
                literature_URI = URIRef(root_prefix.literature + '/' +row[1].split("/")[5].split(".")[0]) # use poster name for literature URI
                resource_URI = URIRef(row[1])
                year = row[10].split("|")[0]

            g.add((literature_URI, dc.title, Literal(title)))
            g.add((literature_URI, bibo.authorList, Literal(author_list_str)))
            g.add((literature_URI, owl.sameAs, resource_URI))
            g.add((literature_URI, bibo.issued, Literal(year)))


    return g

# print read_papers(literature,g)

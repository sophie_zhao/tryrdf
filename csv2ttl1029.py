from rdflib import URIRef, Literal
from rdflib.namespace import RDF, DCTERMS
from rdflib import Graph
from rdflib import Namespace
import uuid
import sys
import csv
maxInt = sys.maxsize
decrement = True

while decrement:
    # decrease the maxInt value by factor 10
    # as long as the OverflowError occurs.
    decrement = False
    try:
        csv.field_size_limit(maxInt)
    except OverflowError:
        maxInt = int(maxInt/10)
        decrement = True

# if the String inside [ ] is not empty, return True
def is_not_empty_array(str):
    if str[1:len(str)-1]:
        return True
    else:
        return False

# extract content inside [ ], make them into a list of strings
def str_to_list(str):
    content = str[1:len(str)-1]
    list_of_str = content.split(",")
    return list_of_str

# extract digestion info, this entry has two layer of [], in the format of [[],[],...], return each digrest child/parent entry as an item in list
def digest_list(str):
    list = str[2:len(str)-3]
    list_of_digest = list.split("],[")
    return list_of_digest

def remove_last_comma(str):
    new_str = str[0:len(str)-1]
    return new_str

def digest_item_per_unit(item):
    cformat = item.replace("Technique:","|").replace("name:","|").replace("enzymes:","|").\
                            replace("retention (GU): ","|").replace("profile ID: ","|").replace("profile name:","|").\
                            replace("profile instrument: ","|").replace("profile dextran standard: ","|")
    list = cformat.split("|")
    return list

def generate_uuid():
    uid_str_full = uuid.uuid4().urn
    uid_str = uid_str_full[9:]
    return uid_str

def csv2ttl(csvfile):
    # -----------------------  set NameSpace   ----------------------------
    # Namespaces in glycan.owl that are missed in rdflib, put here in case of usage later
    bibo = Namespace('http://purl.org/ontology/bibo/')
    glycan = Namespace('http://purl.jp/bio/12/glyco/glycan/')
    dcterms = Namespace('http://purl.org/dc/terms/')
    dc = Namespace('http://purl.org/dc/elements/1.1/')

    # Namespaces for glycobase project
    root_prefix = Namespace('http://rdf.glycobase.org/')
    resource_prefix = Namespace('https://glycobase.nibrt.ie/glycobase/show_glycan.action?glycanSequenceId=')

    # ----------------------  start to construct a graph for input data -------------------
    g = Graph()
    g.bind('glycan',glycan)
    g.bind('bibo',bibo)
    g.bind('dcterms',dcterms)
    g.bind('dc',dc)

    def add_source(graph, structure_URI, source_URI):
        graph.add((structure_URI, glycan.is_from_source, source_URI))
        graph.add((source_URI, RDF.type, glycan.source_nature))
        return graph

    with open(csvfile, 'rb') as csvfile:
        # next(csvfile) # skip the header in the first row
        infile = csv.reader(csvfile, delimiter='\t', quotechar='|')
        for row in infile:
            uoxf = Literal(row[0])
            glycobaseID = Literal(row[1])

            # define URIs in this project
            ref_comp_uuid = generate_uuid()
            ref_comp_URI = URIRef(root_prefix.referencecompound + '/' + ref_comp_uuid)
            structure_URI = URIRef(root_prefix.structure + '/' + glycobaseID)
            resource_prefix = Namespace('https://glycobase.nibrt.ie/glycobase/show_glycan.action?glycanSequenceId=')
            resource_URI = URIRef(resource_prefix + glycobaseID)
            source_URI = URIRef(root_prefix.source + '/' + glycobaseID + generate_uuid())
            reference_URI = URIRef(root_prefix.reference + '/' + ref_comp_uuid)
            ct_sequence_URI = URIRef(root_prefix.structure + '/' + glycobaseID + "/ct")        # fit unicarbkb
            evidence_uplc_URI = URIRef(root_prefix.uplc + '/' + ref_comp_uuid)
            evidence_hplc_URI = URIRef(root_prefix.hplc + '/' + ref_comp_uuid)
            evidence_rpuplc_URI = URIRef(root_prefix.rpuplc + '/' + ref_comp_uuid)
            evidence_ce_URI = URIRef(root_prefix.ce + '/' + ref_comp_uuid)
            lc_peak_URI = URIRef(root_prefix.lc_peak + '/' + generate_uuid())

            # start a statement for structure class
            g.add((structure_URI, RDF.type, glycan.saccharide))
            g.add((structure_URI, glycan.glycosequence, ct_sequence_URI))
            g.add((structure_URI, glycan.has_glycosequence, ct_sequence_URI))
            g.add((structure_URI, glycan.has_resource_entry, resource_URI))
            g.add((structure_URI, glycan.has_uoxf, uoxf))

            # add glycoct sequence
            ct_content = row[2].replace(",","\n")
            ct_sequence = Literal(ct_content)
            g.add((ct_sequence_URI, RDF.type, glycan.glycosequence))
            g.add((ct_sequence_URI, glycan.has_sequence, ct_sequence))
            g.add((ct_sequence_URI, glycan.in_carbohydrate_format, glycan.carbohydrate_format_glycoct))

            # add resource_entry
            g.add((resource_URI, RDF.type, glycan.resource_entry))
            g.add((resource_URI, glycan.in_glycan_database, glycan.database_glycobase))
            g.add((resource_URI, DCTERMS.identifier,glycobaseID))

            # uplc gu, column 4
            if is_not_empty_array(row[3]):
                for item in str_to_list(row[3]):
                    uplc_gu = Literal(item)
                    # add uplc_gu to graph
                    g.add((structure_URI, glycan.has_evidence, evidence_uplc_URI))
                    g.add((evidence_uplc_URI, RDF.type, glycan.evidence_uplc))
                    g.add((evidence_uplc_URI, glycan.has_lc_chromatogram_peak, lc_peak_URI))
                    g.add((lc_peak_URI, glycan.has_glucose_unit, uplc_gu))
                    # print "uplc: " + item

            #hplc gu, c5
            if is_not_empty_array(row[4]):
                for item in str_to_list(row[4]):
                    hplc_gu = Literal(item)
                    # add hplc_gu to graph
                    g.add((structure_URI, glycan.has_evidence, evidence_hplc_URI))
                    g.add((evidence_hplc_URI, RDF.type, glycan.evidence_hplc))
                    g.add((evidence_hplc_URI, glycan.has_lc_chromatogram_peak, lc_peak_URI))
                    g.add((lc_peak_URI, glycan.has_glucose_unit, hplc_gu))
                    # print "hplc: " + item

            #ce gu, c6
            if is_not_empty_array(row[5]):
                for item in str_to_list(row[5]):
                    ce_gu = Literal(item)
                    # add ce_gu to graph
                    g.add((structure_URI, glycan.has_evidence, evidence_ce_URI))
                    g.add((evidence_ce_URI, RDF.type, glycan.evidence_ce))
                    g.add((evidence_ce_URI, glycan.has_glucose_unit, ce_gu))
                    # print "ce: " + item

            #rpuplc gu, c7
            if is_not_empty_array(row[6]):
                for item in str_to_list(row[6]):
                    rpuplc_gu = Literal(item)
                    # add rpuplc_gu to graph
                    g.add((structure_URI, glycan.has_evidence, evidence_rpuplc_URI))
                    g.add((evidence_rpuplc_URI, RDF.type, glycan.evidence_rpuplc))
                    g.add((evidence_rpuplc_URI, glycan.has_lc_chromatogram_peak, lc_peak_URI))
                    g.add((lc_peak_URI, glycan.has_glucose_unit, rpuplc_gu))
                    # print "rpuplc: " + item

            #taxonomy, c8
            # taxonomy and tissue have 1 to 1 relationship, not sure about the relationship to gu value
            if is_not_empty_array(row[7]):
                for item in str_to_list(row[7]):
                    taxonomy = Literal(item)
                    # add taxonomy to graph
                    # g.add((structure_URI, glycan.is_from_source, source_URI))
                    # g.add((source_URI, RDF.type, glycan.source_nature))
                    add_source(g, structure_URI, source_URI)
                    g.add((source_URI, glycan.has_taxon, taxonomy))
                    # print "taxonomy: " + item

            #tissue, c9
            if is_not_empty_array(row[8]):
                for item in str_to_list(row[8]):
                    tissue = Literal(item)
                    # add tissue to graph
                    # g.add((structure_URI, glycan.is_from_source, source_URI))
                    # g.add((source_URI, RDF.type, glycan.source_nature))
                    add_source(g, structure_URI, source_URI)
                    g.add((source_URI, glycan.has_tissue, tissue))
                    # print "tissue: " + item

            #disease, c10
            if is_not_empty_array(row[9]):
                for item in str_to_list(row[9]):
                    disease = Literal(item)
                    # add disease to graph
                    # g.add((structure_URI, glycan.is_from_source, source_URI))
                    # g.add((source_URI, RDF.type, glycan.source_nature))
                    add_source(g, structure_URI, source_URI)
                    g.add((source_URI, glycan.has_disease, disease))
                    # print "disease: " + item

            #report title, c11 e.g. published glycans /CE Database /NIBRT Public Collection
            # gu-report-sample have 1 to 1 relationship
            if is_not_empty_array(row[10]):
                for item in str_to_list(row[10]):
                    report_title = Literal(item)
                    # add report title to graph
                    g.add((reference_URI, RDF.type, bibo.Article))
                    g.add((reference_URI, dc.title, report_title))
                    # print "report_title: " + item

            # #report_id, c12
            # if is_not_empty_array(row[11]):
            #     for item in str_to_list(row[11]):
            #         report_id = Literal(item)
            #         # add report_id to graph
            #         print "report_id: " + item

            #sample_title, c13 e.g. cow /sheep /Professor Rudd NIBRT Data /Human Serum
            if is_not_empty_array(row[12]):
                for item in str_to_list(row[12]):
                    sample_title = Literal(item)
                    # add sample_title to graph
                    g.add((reference_URI, RDF.type, bibo.Article))
                    g.add((reference_URI, bibo.shortTitle, sample_title))
                    # print "sample_title: " + item

            # #sample_id, c14
            # if is_not_empty_array(row[13]):
            #     for item in str_to_list(row[13]):
            #         sample_id = Literal(item)
            #         # add sample_id to graph
            #         # print "sample_id: " + item

            # #profile_title, c15. Not informative, suggest to omit
            # if is_not_empty_array(row[14]):
            #     for item in str_to_list(row[14]):
            #         profile_title = Literal(item)
            #         # add profile_title to graph
            #         print "profile_title: " + item
            #
            # #profile_id, c16. Not informative, suggest to omit
            # if is_not_empty_array(row[15]):
            #     for item in str_to_list(row[15]):
            #         profile_id = Literal(item)
            #         # add disease to graph
            #         print "profile_id: " + item

            #digestion_children, c17
            if is_not_empty_array(row[16]):
                for item in digest_list(row[16]):
                    # cformat = item.replace("Technique:","|").replace("name:","|").replace("enzymes:","|").\
                    #         replace("retention (GU): ","|").replace("profile ID: ","|").replace("profile name:","|").\
                    #         replace("profile instrument: ","|").replace("profile dextran standard: ","|")
                    # array_in_item = cformat.split("|")
                    array_in_item = digest_item_per_unit(item)

                    digest_child_id = remove_last_comma(array_in_item[0])
                    child_evidence_type = remove_last_comma(array_in_item[1])
                    child_uoxf = remove_last_comma(array_in_item[2])
                    child_enzyme = remove_last_comma(array_in_item[3])
                    child_retention = remove_last_comma(array_in_item[4])
                    # child_profile_id = remove_last_comma(array_in_item[5])
                    # child_profile_name = remove_last_comma(array_in_item[6])
                    child_profile_instrument = remove_last_comma(array_in_item[7])
                    child_pf_dextran_sd = remove_last_comma(array_in_item[8])

                    childID = Literal(digest_child_id)
                    childEvidenceType = Literal(child_evidence_type)
                    childUOXF = Literal(child_uoxf)
                    childEnzyme = Literal(child_enzyme)
                    childRetention = Literal(child_retention)
                    childPrflInstr = Literal(child_profile_instrument)
                    childPrflDextran = Literal(child_pf_dextran_sd)

                    # generate uuid for child
                    uid_str_full = uuid.uuid4().urn
                    uid_str = uid_str_full[9:]
                    # add digest child to current structure with "has_compound"
                    digest_child_URI = URIRef(root_prefix + glycobaseID + "/digest_child/"+ childID)
                    child_evidence_URI = URIRef(root_prefix + glycobaseID + "/digest_child/"+ childID + "/evidence/" + uid_str)
                    g.add((structure_URI, glycan.has_compound, digest_child_URI))
                    g.add((digest_child_URI, RDF.type, glycan.saccharide))              # just like a new structure statement
                    g.add((digest_child_URI, glycan.has_evidence, child_evidence_URI))
                    g.add((child_evidence_URI, glycan.digest_by_enzyme, childEnzyme))   # To be decided
                    g.add((child_evidence_URI, glycan.instrument, childPrflInstr))
                    g.add((child_evidence_URI, glycan.dextran_standard, childPrflDextran))
                    g.add((child_evidence_URI, glycan.evidence_type, childEvidenceType))
                    g.add((child_evidence_URI, glycan.has_glucose_units, childRetention))

            #digestion_parent, c18
            if is_not_empty_array(row[17]):
                for item in digest_list(row[17]):
                    # cformat = item.replace("Technique:","|").replace("name:","|").replace("enzymes:","|").\
                    #         replace("retention (GU): ","|").replace("profile ID: ","|").replace("profile name:","|").\
                    #         replace("profile instrument: ","|").replace("profile dextran standard: ","|")
                    # array_in_item = cformat.split("|")
                    array_in_item = digest_item_per_unit(item)

                    digest_parent_id = remove_last_comma(array_in_item[0])
                    parent_evidence_type = remove_last_comma(array_in_item[1])
                    parent_uoxf = remove_last_comma(array_in_item[2])
                    parent_enzyme = remove_last_comma(array_in_item[3])
                    parent_retention = remove_last_comma(array_in_item[4])
                    # parent_profile_id = remove_last_comma(array_in_item[5])
                    # parent_profile_name = remove_last_comma(array_in_item[6])
                    parent_profile_instrument = remove_last_comma(array_in_item[7])
                    parent_pf_dextran_sd = remove_last_comma(array_in_item[8])

                    parentID = Literal(digest_parent_id)
                    parentEvidenceType = Literal(parent_evidence_type)
                    parentUOXF = Literal(parent_uoxf)
                    parentEnzyme = Literal(parent_enzyme)
                    parentRetention = Literal(parent_retention)
                    parentPrflInstr = Literal(parent_profile_instrument)
                    parentPrflDextran = Literal(parent_pf_dextran_sd)

                    # generate uuid for parent
                    uid_str_full = uuid.uuid4().urn
                    uid_str = uid_str_full[9:]

                    # add digest parent to current structure with "has_compound"
                    digest_parent_URI = URIRef(root_prefix + glycobaseID + "/digest_parent/"+ parentID)
                    parent_evidence_URI = URIRef(root_prefix + glycobaseID + "/digest_parent/"+ parentID + "/evidence/" + uid_str)
                    g.add((structure_URI, glycan.has_compound, digest_parent_URI))
                    g.add((digest_parent_URI, RDF.type, glycan.saccharide))              # just like a new structure statement
                    g.add((digest_parent_URI, glycan.has_evidence, parent_evidence_URI))
                    g.add((parent_evidence_URI, glycan.digest_by_enzyme, parentEnzyme))   # To be decided
                    g.add((parent_evidence_URI, glycan.instrument, parentPrflInstr))
                    g.add((parent_evidence_URI, glycan.dextran_standard, parentPrflDextran))
                    g.add((parent_evidence_URI, glycan.evidence_type, parentEvidenceType))
                    g.add((parent_evidence_URI, glycan.has_glucose_units, parentRetention))


        # print g.serialize(format = 'turtle')
        return g.serialize(format = 'turtle')

demo_file = '/home/sophie/Desktop/demo.csv'
full_file = '/home/sophie/Desktop/glycobase_spread.csv'
macfile = '/Users/Sophie/Dropbox/MQU/demo1029.csv'

print csv2ttl(macfile)

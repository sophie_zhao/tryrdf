import requests

# check unicorn for matching glycoct
# get json return from unicarbkb
# only for N-linked glycans
def matchGlycoCT(glycoCT):
    # base_url = 'http://localhost:9000/rest/unicorn/structureMass'
    base_url='http://www.unicarbkb.org/rest/unicorn/structureMass'
    #notice format below
    # payload = {'glycoct': "RES\n1b:b-dglc-HEX-1:5\n2s:n-acetyl\n3b:b-dglc-HEX-1:5\n4s:n-acetyl\n5b:b-dman-HEX-1:5\n6b:a-dman-HEX-1:5\n7b:a-dman-HEX-1:5\nLIN\n1:1d(2+1)2n\n2:1o(4+1)3d\n3:3d(2+1)4n\n4:3o(4+1)5d\n5:5o(3+1)6d\n6:5o(6+1)7d"}
    payload = {'glycoct':glycoCT}
    result = requests.get(base_url, params=payload)
    # print result.content
    # if result.ok:
    if len(result.content) > 18:
        dict = result.content
        average_mass = dict.split(',')[2].split(':')[1]
        mono_mass = dict.split(',')[3].split(':')[1][:-1]
        mass_list = [average_mass,mono_mass]
        return mass_list
    else:
        return "NA"

# payload test structures

# res = matchGlycoCT("RES\n1b:b-dglc-HEX-1:5\n2s:n-acetyl\n3b:b-dglc-HEX-1:5\n4s:n-acetyl\n5b:b-dman-HEX-1:5\n6b:a-dman-HEX-1:5\n7b:a-dman-HEX-1:5\nLIN\n1:1d(2+1)2n\n2:1o(4+1)3d\n3:3d(2+1)4n\n4:3o(4+1)5d\n5:5o(3+1)6d\n6:5o(6+1)7d")
#res = json.loads(request1())
# res=matchGlycoCT("RES\n1b:b-dglc-HEX-1:5\n2s:n-acetyl\n3b:a-dglc-HEX-1:5\n4s:n-acetyl\n5b:b-dman-HEX-1:5\n6b:a-dman-HEX-1:5\n7b:a-dman-HEX-1:5\n8b:a-dman-HEX-1:5\n9b:a-dglc-HEX-1:5\n10b:a-dglc-HEX-1:5\n11b:a-dglc-HEX-1:5\n12b:a-dman-HEX-1:5\n13b:a-dman-HEX-1:5\n14b:a-dman-HEX-1:5\nLIN\n1:1d(2+1)2n\n2:1o(4+1)3d\n3:3d(2+1)4n\n4:3o(4+1)5d\n5:5o(3+1)6d\n6:6o(2+1)7d\n7:7o(2+1)8d\n8:8o(3+1)9d\n9:9o(3+1)10d\n10:10o(2+1)11d\n11:5o(6+1)12d\n12:12o(3+1)13d\n13:12o(6+1)14d")
# res = matchGlycoCT("RES\n1b:b-dglc-HEX-1:5\n2s:n-acetyl\n3b:b-dglc-HEX-1:5\n4s:n-acetyl\n5b:b-dman-HEX-1:5\n6b:a-dman-HEX-1:5\n7b:b-dglc-HEX-1:5\n8s:n-acetyl\n9b:b-dglc-HEX-1:5\n10s:n-acetyl\n11b:a-dman-HEX-1:5\n12b:b-dglc-HEX-1:5\n13s:n-acetyl\nLIN\n1:1d(2+1)2n\n2:1o(4+1)3d\n3:3d(2+1)4n\n4:3o(4+1)5d\n5:5o(3+1)6d\n6:6o(2+1)7d\n7:7d(2+1)8n\n8:5o(4+1)9d\n9:9d(2+1)10n\n10:5o(6+1)11d\n11:11o(2+1)12d\n12:12d(2+1)13n\nUND\nUND1:100.0:100.0\nParentIDs:7|12\nSubtreeLinkageID1:o(4+1)d\nRES\n14b:b-dgal-HEX-1:5")
# res = matchGlycoCT("RES\n1b:b-dglc-HEX-1:5\n2s:n-acetyl\n3b:b-dglc-HEX-1:5\n4s:n-acetyl\n5b:b-dman-HEX-1:5\n6b:a-dman-HEX-1:5\n7b:b-dglc-HEX-1:5\n8s:n-acetyl\n9b:b-dgal-HEX-1:5\n10b:a-dgro-dgal-NON-2:6|1:a|2:keto|3:d\n11s:n-acetyl\n12b:b-dglc-HEX-1:5\n13s:n-acetyl\n14b:b-dgal-HEX-1:5\n15b:a-dgro-dgal-NON-2:6|1:a|2:keto|3:d\n16s:n-acetyl\n17b:a-dman-HEX-1:5\n18b:b-dglc-HEX-1:5\n19s:n-acetyl\n20b:b-dgal-HEX-1:5\n21b:a-dgro-dgal-NON-2:6|1:a|2:keto|3:d\n22s:n-acetyl\n23b:b-dglc-HEX-1:5\n24s:n-acetyl\n25b:b-dgal-HEX-1:5\n26b:a-dgro-dgal-NON-2:6|1:a|2:keto|3:d\n27s:n-acetyl\n28b:a-dgal-HEX-1:5|6:d\nLIN\n1:1d(2+1)2n\n2:1o(4+1)3d\n3:3d(2+1)4n\n4:3o(4+1)5d\n5:5o(3+1)6d\n6:6o(2+1)7d\n7:7d(2+1)8n\n8:7o(4+1)9d\n9:9o(3+2)10d\n10:10d(5+1)11n\n11:6o(4+1)12d\n12:12d(2+1)13n\n13:12o(4+1)14d\n14:14o(3+2)15d\n15:15d(5+1)16n\n16:5o(6+1)17d\n17:17o(2+1)18d\n18:18d(2+1)19n\n19:18o(4+1)20d\n20:20o(3+2)21d\n21:21d(5+1)22n\n22:17o(6+1)23d\n23:23d(2+1)24n\n24:23o(4+1)25d\n25:25o(3+2)26d\n26:26d(5+1)27n\n27:1o(6+1)28d");
#
# print res


from rdflib import Namespace

class GlycobaseName:

    bibo = Namespace('http://purl.org/ontology/bibo/')
    glycan = Namespace('http://purl.jp/bio/12/glyco/glycan/')
    dcterms = Namespace('http://purl.org/dc/terms/')
    dc = Namespace('http://purl.org/dc/elements/1.1/')

    # Namespaces for glycobase project
    root_prefix = Namespace('http://rdf.glycobase.org/')
    resource_prefix = Namespace('https://glycobase.nibrt.ie/glycobase/show_glycan.action?glycanSequenceId=')
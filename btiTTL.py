from rdflib import URIRef, Literal
from rdflib.namespace import RDF, DCTERMS, XSD
from rdflib import Graph
from prefix import glycan, root_prefix, dc
import uuid
import sys, os
import glob
import csv
maxInt = sys.maxsize
decrement = True

missing_mass=['1994', '27387', '27496', '27498', '3122', '27715', '27804', '12150', '27893', '27895', '27896',
              '27898', '27899', '27402', '27808', '27809', '30166', '30194']

while decrement:
    # decrease the maxInt value by factor 10
    # as long as the OverflowError occurs.
    decrement = False
    try:
        csv.field_size_limit(maxInt)
    except OverflowError:
        maxInt = int(maxInt/10)
        decrement = True

def generate_uuid():
    uid_str_full = uuid.uuid4().urn
    uid_str = uid_str_full[9:]
    return uid_str

def check_taxon(species): # Analytics group now only analyze CHO or xx expressed Human protein
    if species.lower() == 'human':
        return 'Homo sapiens'.upper()
    elif species.lower() == 'mouse':
        return 'Mus musculus'.upper()


def btiTTL(graph,metafile,current_dir,profile_id,sample_id):

    # ----------------------  start to construct a graph for input data -------------------
    # g = Graph()
    # g.bind('glycan',glycan)
    g = graph
    # print 'metafile: ' + metafile
    with open(metafile, 'rb') as f:
        first_line = f.readline().rstrip() # rstrip() remove the '\n' character
        header = first_line.split('\t')
        infile = csv.reader(f, delimiter='\t', quotechar='|')
        for row in infile: # for each line of meta data
            d = dict(zip(header,row))
            # print d['date']
            # exp_data_file = '/home/sophie/Dropbox/workfile/bti2ttl/susanto_exoglycosidase_AAT/id_data_' + d['sample_name'] + '.txt'
            file_name = "id_data_" + d['sample_name'] + '.txt'
            exp_data_file = os.path.join(current_dir,file_name)

            with open(exp_data_file, 'rb') as csvfile:
                '''
                each data file is a profile/sample, profile_id and sample_id should increase after reading each data file.

                '''
                next(csvfile) # skip the header in the first row
                data_file = csv.reader(csvfile, delimiter='\t', quotechar='|')

                # record previous uoxf name for comparison of isomer
                prev_uoxf = ''
                name_counter = 1
                label_delta_mass = 120.0688 # net mass that increased by adding 2-AB

                # record previous elution area for addition of co-eluting structure, consider only two co-eluting structures
                na_counter = 0

                for row in data_file:
                    uoxf = row[0]
                    glycobase_id = row[1] # str;
                    amount = row[2] # relative amount %
                    obsGu = row[4]  # observed GU
                    expMS = row[5]  # str; expected mass with label, could be empty
                    obsMS = row[6]  # observed m/z with label, could be empty
                    charge = row[7] # could be empty
                    # print 'glycan: '+uoxf

                    if charge and obsMS: # if mass confirmed, then write evidence
                        # define URIs using ID, glycobase_id below 30000 is from GlycoBase, above is new structure
                        structure_URI = URIRef(root_prefix.structure + '/' + glycobase_id)
                        # start a statement for structure class
                        g.add((structure_URI, RDF.type, glycan.saccharide))
                        g.add((structure_URI, RDF.type, glycan.N_glycan))
                        g.add((structure_URI, glycan.has_uoxf, Literal(uoxf)))
                        g.add((structure_URI, glycan.has_glycobase_id,Literal(glycobase_id,datatype=XSD.integer)))

                        # add resource_entry
                        resource_URI = URIRef(root_prefix + glycobase_id)
                        g.add((structure_URI, glycan.has_resource_entry, resource_URI))
                        g.add((resource_URI, RDF.type, glycan.resource_entry))
                        g.add((resource_URI, DCTERMS.identifier,Literal(glycobase_id)))
                        g.add((resource_URI, glycan.in_glycan_database, glycan.database_glycostore))

                        ## reference compound
                        if uoxf == prev_uoxf:
                            name_counter += 1
                            evidence_identifier = glycobase_id + '-' + str(profile_id) + '-' + str(name_counter)
                            # ref_comp_URI = URIRef(root_prefix.referencecompound + '/' + glycobase_id + '-' + str(profile_id)
                            #                       + '-' + str(name_counter))
                        else:
                            name_counter = 1
                            evidence_identifier = glycobase_id + '-' + str(profile_id)

                        ref_comp_URI = URIRef(root_prefix.referencecompound + '/' + evidence_identifier)
                        g.add((ref_comp_URI, RDF.type, glycan.referenced_compound))
                        g.add((ref_comp_URI, glycan.has_glycan, structure_URI))
                        g.add((ref_comp_URI, glycan.is_from_report, Literal('BTI N-glycans')))
                        # ------------------- source URI -------------------------
                        source_URI = URIRef(root_prefix.source + '/' + str(sample_id))
                        g.add((ref_comp_URI, glycan.is_from_source, source_URI))
                        g.add((source_URI, RDF.type, glycan.source_nature))
                        g.add((source_URI, glycan.from_sample, Literal(d['sample_name'])))
                        if check_taxon(d['taxonomy']):
                            g.add((source_URI, glycan.has_taxon, Literal(check_taxon(d['taxonomy']))))
                        g.add((source_URI, glycan.has_sample_type, Literal(d['bio_source'])))
                        if d['is_recombinant'].lower() == 'yes':
                            g.add((source_URI, glycan.hosted_by, Literal(d['cell_type'])))
                        if d['disease'].lower() != 'no'and d['disease'] != '-':
                            g.add((source_URI, glycan.has_disease, Literal(d['disease'])))
                        # -------------------- evidence_ms ------------------------
                        ref_ms_URI = URIRef(root_prefix.evidence_ms + '/'+ evidence_identifier)
                        g.add((ref_comp_URI, glycan.has_evidence,ref_ms_URI))
                        g.add((ref_ms_URI, glycan.ms1_verified, Literal('yes')))
                        g.add((ref_ms_URI, glycan.has_charge, Literal(charge,datatype=XSD.integer)))
                        g.add((ref_ms_URI, glycan.observed_mass_with_label, Literal(obsMS,datatype=XSD.float)))

                        if int(glycobase_id) > 30001 or glycobase_id in missing_mass:
                            if expMS:
                                mono_mass = float(expMS) - label_delta_mass
                                g.add((structure_URI, glycan.has_monoisotopic_molecular_weight, Literal(mono_mass, datatype=XSD.float)))
                            else:
                                print glycobase_id, " is missing monoisotopic mass"

                        # ------------------- lc_chromatogram_peak ------------------
                        lc_peak_URI = URIRef(root_prefix.lc_peak + '/' + evidence_identifier)
                        g.add((ref_comp_URI, glycan.has_lc_chromatogram_peak, lc_peak_URI))
                        g.add((structure_URI, glycan.has_lc_chromatogram_peak, lc_peak_URI))
                        evidence_uplc_URI = URIRef(root_prefix.uplc + '/' + str(profile_id))
                        g.add((ref_comp_URI,glycan.is_from_profile, evidence_uplc_URI))
                        g.add((evidence_uplc_URI, RDF.type, glycan.evidence_uplc))
                        g.add((evidence_uplc_URI, glycan.has_lc_chromatogram_peak, lc_peak_URI))
                        g.add((evidence_uplc_URI, glycan.used_column, Literal(d['column'])))
                        g.add((evidence_uplc_URI, glycan.used_instrument, Literal(d['instrument'])))
                        g.add((evidence_uplc_URI, glycan.used_standard, Literal(d['calibration_standard'])))
                        g.add((evidence_uplc_URI, glycan.has_label, Literal(d['label'])))
                        g.add((evidence_uplc_URI, glycan.release_method, Literal(d['release_method'])))
                        g.add((evidence_uplc_URI, dc.date, Literal(d['date'],datatype=XSD.date)))
                        g.add((evidence_uplc_URI, dc.operator, Literal(d['operator'])))
                        g.add((evidence_uplc_URI, glycan.contributed_by, Literal('BTI-Analytics')))
                        g.add((lc_peak_URI, glycan.has_glucose_unit, Literal(obsGu,datatype=XSD.float)))
                        if amount != 'NA': # co-eluting structures share this value, so manually changed to 'NA'
                            g.add((lc_peak_URI, glycan.has_peak_area, Literal(amount)))
                        else:
                            na_counter += 1
                            if na_counter > 1:
                                g.add((structure_URI, glycan.has_co_eluting_structure, prev_structure_URI))
                                g.add((prev_structure_URI,glycan.has_co_eluting_structure, structure_URI))
                                na_counter = 0

                        # -------------------- source protein -------------------------
                        protein_id = 0
                        if d['bio_source'].lower() == 'glycoprotein':
                            if d['protein_name'] == 'IgG':
                                protein_id = 87
                            elif d['protein_name'] == 'RNAse B':
                                protein_id = 86
                            elif d['protein_name'] == 'Transferrin':
                                protein_id = 89
                            elif d['protein_name'] == 'Haptoglobin':
                                protein_id = 90
                            elif d['protein_name'] == 'AAT':
                                protein_id = 91
                            else:
                                protein_id = sample_id
                            protein_URI = URIRef(root_prefix.proteinsummary + '/'+ str(protein_id))
                            g.add((protein_URI, glycan.has_protein_name, Literal(d['protein_name'])))
                            g.add((protein_URI, glycan.has_attached_glycan, structure_URI))


                        # finished reading current line
                        prev_uoxf = uoxf
                        prev_structure_URI = structure_URI
                    # else: # when ms is not confirmed, drop this record
                    #     g.add((ref_ms_URI, glycan.ms1_verified, Literal('no')))

            profile_id += 1
            sample_id += 1

        print profile_id
        print sample_id
        # return g.serialize(format = 'turtle')
        return (g,profile_id,sample_id)


# -------------------------------------- automate file writing in all directory -------------------------
directory_origin = '/home/sophie/Dropbox/workfile/bti2ttl/'
destination_file = '/home/sophie/Dropbox/workfile/glycostore/bti_N_glycan_20180130.ttl'
profileID = 2001
sampleID = 201

def write_bti_ttl(parent_dir,destination_file,profile_id,sample_id):
    current, dirs, files = os.walk(parent_dir).next()
    f = open(destination_file,'w')
    graph = Graph()
    graph.bind('glycan',glycan)
    for dir in dirs:
        print 'dir: '+dir
        current_abs_dir = os.path.join(parent_dir,dir)
        # although it's for loop, only one meta file in each sub-dir
        for file in glob.glob(os.path.join(current_abs_dir,'meta-header_*.txt')):
            print 'file: ' +file

            # execute the function
            run_dir_result = btiTTL(graph,file,current_abs_dir,profile_id,sample_id)
            # update parameters
            graph = run_dir_result[0]
            profile_id = run_dir_result[1]
            sample_id = run_dir_result[2]

    # finish reading single meta file and converting all data files into ttl
    #finish for current dir
    f.write(graph.serialize(format = 'turtle'))
    f.close()

write_bti_ttl(directory_origin,destination_file,profileID,sampleID)



# profileID= 2001 # Chenchen 1 file used
# sampleID = 201
# profileID = 2002   # YS_LTC batch 10 files used
# sampleID = 202
# profileID = 2012   # YS_DG44 batch 8 files used
# sampleID = 212
# profileID = 2020   # WL_HER batch 12 files used
# sampleID = 220
# profileID = 2032   # YS_marker batch 10 files used
# sampleID = 232
# profileID = 2042   # cell culture batch 7 files used
# sampleID = 242
# profileID = 2049   # AAT exoglycosidase batch 4 files used
# sampleID = 249
# profileID = 2053     # daniel mouse serum sample 1 file used
# sampleID = 253
# profileID = 2054  # for next usage
# sampleID = 254

# meta_file1 = '/home/sophie/Dropbox/workfile/bti2ttl/susanto_YS_LTC/meta-header_YS_LTC.txt'
# meta_file2 ='/home/sophie/Dropbox/workfile/bti2ttl/susanto_YS_DG44/meta-header_YS_DG44.txt'
# meta_file3 = '/home/sophie/Dropbox/workfile/bti2ttl/susanto_WL_HER/meta-header_WL1023HER.txt'
# meta_file4 = '/home/sophie/Dropbox/workfile/bti2ttl/susanto_YS_marker/meta-header_YS_additive_and_selection_marker.txt'
# meta_file5 = '/home/sophie/Dropbox/workfile/bti2ttl/susanto_cell_culture/meta-header_cell_culture.txt'
# meta_file6 = '/home/sophie/Dropbox/workfile/bti2ttl/susanto_exoglycosidase_AAT/meta-header_exoglycosidase_AAT.txt'
# meta_daniel = '/home/sophie/Dropbox/workfile/bti2ttl/Daniel_MouseSerum_UNIFI/meta-header-mouse-serum.txt'
# meta_chen = '/home/sophie/Dropbox/workfile/bti2ttl/ChenChen_human_serum/meta-data-header.txt'
# tofile = '/home/sophie/Desktop/bti-chen.ttl'

# ttlFile = '/home/sophie/Dropbox/workfile/bti2ttl/human_serum_Chen_161005.ttl'
# ttlFile = '/home/sophie/Dropbox/workfile/bti2ttl/susanto_exoglycosidase_AAT_161024.ttl'
# ttlFile = '/home/sophie/Desktop/susanto_YS_DG44.ttl'

# f = open(ttlFile,'w')
# f.write(btiTTL(meta_file6,profileID,sampleID))
# f.close()


from rdflib import Namespace

# Namespaces in glycan.owl that are missed in rdflib, put here in case of usage later
glycan = Namespace('http://purl.jp/bio/12/glyco/glycan/')
bibo = Namespace('http://purl.org/ontology/bibo/')
dcterms = Namespace('http://purl.org/dc/terms/')
dc = Namespace('http://purl.org/dc/elements/1.1/')
owl = Namespace('http://www.w3.org/2002/07/owl#')

# Namespaces for glycobase project
# root_prefix = Namespace('http://rdf.glycobase.org/')
root_prefix = Namespace('http://rdf.glycostore.org/')
# resource_prefix = Namespace('https://glycobase.nibrt.ie/glycobase/show_glycan.action?glycanSequenceId=')
pubmed_prefix = Namespace('http://www.ncbi.nlm.nih.gov/pubmed/')

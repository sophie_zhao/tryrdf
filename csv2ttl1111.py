from rdflib import URIRef, Literal
from rdflib.namespace import RDF, DCTERMS, XSD
from rdflib import Graph
from prefix import glycan, root_prefix,resource_prefix, bibo,dc,dcterms,owl
from digestion import process_digest
from literature import read_literature, read_papers
from source import add_source, add_source_protein
from unicornRestMass import matchGlycoCT
import uuid
import sys
import csv
import timeit
start = timeit.default_timer()
maxInt = sys.maxsize
decrement = True

while decrement:
    # decrease the maxInt value by factor 10
    # as long as the OverflowError occurs.
    decrement = False
    try:
        csv.field_size_limit(maxInt)
    except OverflowError:
        maxInt = int(maxInt/10)
        decrement = True

# if the String inside [ ] is not empty, return True
def is_not_empty_array(str):
    if str[1:len(str)-1]:
        return True
    else:
        return False

# extract content inside [ ], make them into a list of strings
def str_to_list(str):
    content = str[1:len(str)-1]
    list_of_str = content.split(",")
    return list_of_str

def remove_last_comma(str):
    new_str = str[0:len(str)-1]
    return new_str

def generate_uuid():
    uid_str_full = uuid.uuid4().urn
    uid_str = uid_str_full[9:]
    return uid_str

def csv2ttl(csvfile):

    # ----------------------  start to construct a graph for input data -------------------
    g = Graph()
    g.bind('glycan',glycan)
    g.bind('bibo',bibo)
    g.bind('dcterms',dcterms)
    g.bind('dc',dc)
    g.bind('owl', owl)

    ref_gu_list = []
    ref_ms2_list = []
    ref_ms1_list = []
    ref_pubmedID_list =[]

    # construct literature graph from literature files
    g = read_papers('/home/sophie/Dropbox/MQU/pubmed_result.csv',g)

    with open(csvfile, 'rb') as csvfile:
        next(csvfile) # skip the header in the first row
        infile = csv.reader(csvfile, delimiter='\t', quotechar='|')
        i = 0
        for row in infile:
            uoxf = row[0]
            print 'reading ' +str(i) + " row..."
            print uoxf
            glycobase_id = row[1]

            # define URIs using glycobaseID for outer linkage
            structure_URI = URIRef(root_prefix.structure + '/' + glycobase_id)
            resource_URI = URIRef(resource_prefix + glycobase_id)
            ct_sequence_URI = URIRef(root_prefix.structure + '/' + glycobase_id + "/ct")        # fit unicarbkb

            # start a statement for structure class
            g.add((structure_URI, RDF.type, glycan.saccharide))
            g.add((structure_URI, glycan.glycosequence, ct_sequence_URI))
            g.add((structure_URI, glycan.has_glycosequence, ct_sequence_URI))
            g.add((structure_URI, glycan.has_resource_entry, resource_URI))
            g.add((structure_URI, glycan.has_uoxf, Literal(uoxf)))
            g.add((structure_URI, glycan.has_glycobase_id,Literal(glycobase_id,datatype=XSD.integer)))

            # add glycoct sequence
            ct_content = row[2].replace(",","\n")
            # add mass from rest query of unicarbkb
            mass_list = matchGlycoCT(ct_content)
            # print mass_list
            if mass_list != "NA":
                g.add((structure_URI, glycan.has_average_mass, Literal(mass_list[0],datatype=XSD.float)))
                g.add((structure_URI, glycan.has_mono_mass, Literal(mass_list[1],datatype=XSD.float)))

            ct_sequence = Literal(ct_content)
            g.add((ct_sequence_URI, RDF.type, glycan.glycosequence))
            g.add((ct_sequence_URI, glycan.has_sequence, ct_sequence))
            g.add((ct_sequence_URI, glycan.in_carbohydrate_format, glycan.carbohydrate_format_glycoct))

            # add resource_entry
            g.add((resource_URI, RDF.type, glycan.resource_entry))
            g.add((resource_URI, glycan.in_glycan_database, glycan.database_glycobase))
            g.add((resource_URI, DCTERMS.identifier,Literal(glycobase_id)))

            # -------------- add paper reference------------------
            #literature, c13
            if is_not_empty_array(row[12]):
                literature_content = read_literature(row[12])
                ref_gu_list = literature_content[0]
                ref_ms2_list = literature_content[1]
                ref_ms1_list = literature_content[2]
                ref_pubmedID_list = literature_content[3]

            # -------------------------  read in evidence -----------------------------------
            # uplc gu, column 4
            if is_not_empty_array(row[3]):
                entry = row[3]
                content = entry[2:len(entry)-2]
                list_gu = content.split("], [Glucose")
                profile_id = 'NA'
                result = []
                for item in list_gu:
                    list = item.split(":")

                    gu = list[1].split(",")[0]
                    area = list[2].split(",")[0]
                    number = list[3].split(",")[0] # could be "null"
                    report = list[4].split(",")[0]
                    report_id = list[5].split(",")[0]
                    sample = list[6].split(",")[0]
                    if len(list) > 8:
                        sample_id = list[7].split(",")[0]
                        profile_id = list[9].split(",")[0].strip()
                    else:
                        sample_id = list[7]

                    # # print gu + '\t' + area + '\t'+ number +'\t' + report + '\t'+report_id + '\t' + sample + '\t'+ sample_id + '\t' + profile_id
                    if profile_id == 'NA':
                        profile_id = generate_uuid()

                    ref_comp_URI = URIRef(root_prefix.referencecompound + '/' + glycobase_id + '-' + profile_id)

                    g.add((ref_comp_URI, RDF.type, glycan.referenced_compound))
                    g.add((ref_comp_URI, glycan.has_glycan, structure_URI))
                    g.add((ref_comp_URI, glycan.is_from_report, Literal(report)))
                    add_source(g,sample,sample_id,ref_comp_URI)
                    add_source_protein(g,sample,sample_id,structure_URI)

                    evidence_uplc_URI = URIRef(root_prefix.uplc + '/' + profile_id)
                    g.add((ref_comp_URI, glycan.is_from_profile, evidence_uplc_URI))
                    g.add((evidence_uplc_URI, RDF.type, glycan.evidence_uplc))
                    g.add((evidence_uplc_URI, glycan.has_label, Literal("2-AB")))
                    g.add((evidence_uplc_URI, glycan.contributed_by, Literal("GlycoBase")))

                    lc_peak_URI = URIRef(root_prefix.lc_peak + '/' + glycobase_id+'-'+profile_id)
                    g.add((evidence_uplc_URI, glycan.has_lc_chromatogram_peak, lc_peak_URI))
                    g.add((ref_comp_URI, glycan.has_lc_chromatogram_peak, lc_peak_URI))
                    g.add((structure_URI,glycan.has_lc_chromatogram_peak, lc_peak_URI))
                    g.add((lc_peak_URI, glycan.has_glucose_unit, Literal(gu)))

                    if area != 'null':
                        g.add((lc_peak_URI, glycan.has_peak_area, Literal(area)))
                    if number != 'null':
                        g.add((lc_peak_URI,glycan.has_peak_number,Literal(number)))

            #hplc gu, c5
            if is_not_empty_array(row[4]):
                entry = row[4]
                content = entry[2:len(entry)-2]
                list_gu = content.split("], [Glucose")
                profile_id = 'NA'
                for item in list_gu:
                    list = item.split(":")

                    gu = list[1].split(",")[0]
                    area = list[2].split(",")[0]
                    number = list[3].split(",")[0] # could be "null"
                    report = list[4].split(",")[0]
                    report_id = list[5].split(",")[0]
                    sample = list[6].split(",")[0]
                    if len(list) > 8:
                        sample_id = list[7].split(",")[0]
                        profile_id = list[9].split(",")[0].strip()
                    else:
                        sample_id = list[7]

                    if profile_id == 'NA':
                        profile_id = generate_uuid()

                    ref_comp_URI = URIRef(root_prefix.referencecompound + '/' + glycobase_id + '-' + profile_id)

                    g.add((ref_comp_URI, RDF.type, glycan.referenced_compound))
                    g.add((ref_comp_URI, glycan.has_glycan, structure_URI))
                    g.add((ref_comp_URI, glycan.is_from_report, Literal(report)))

                    add_source(g,sample,sample_id,ref_comp_URI)
                    add_source_protein(g,sample,sample_id,structure_URI)

                    evidence_hplc_URI = URIRef(root_prefix.hplc + '/' + profile_id)
                    g.add((ref_comp_URI, glycan.is_from_profile, evidence_hplc_URI))
                    g.add((evidence_hplc_URI, RDF.type, glycan.evidence_hplc))
                    g.add((evidence_hplc_URI, glycan.has_label, Literal("2-AB")))
                    g.add((evidence_hplc_URI, glycan.contributed_by, Literal("GlycoBase")))

                    lc_peak_URI = URIRef(root_prefix.lc_peak + '/' + glycobase_id+'-'+profile_id)
                    g.add((evidence_hplc_URI, glycan.has_lc_chromatogram_peak, lc_peak_URI))
                    g.add((ref_comp_URI, glycan.has_lc_chromatogram_peak, lc_peak_URI))
                    g.add((structure_URI, glycan.has_lc_chromatogram_peak, lc_peak_URI))
                    g.add((lc_peak_URI, glycan.has_glucose_unit, Literal(gu)))

                    if area != 'null':
                        g.add((lc_peak_URI, glycan.has_peak_area, Literal(area)))
                    if number != 'null':
                        g.add((lc_peak_URI,glycan.has_peak_number,Literal(number)))

                    def check_ref_gu(evidence_gu,ref_gu_list):
                        i = 0
                        while i < len(ref_gu_list):
                            if ref_gu_list[i].strip() == evidence_gu:
                                return i
                            i += 1
                        return 100
                    index = check_ref_gu(gu,ref_gu_list)
                    if index != 100:
                        literature_URI = URIRef(root_prefix.reference + '/' +ref_pubmedID_list[index])
                        ref_ms_URI = URIRef(root_prefix.evidence_ms + '/'+ profile_id)
                        g.add((ref_comp_URI, glycan.published_in, literature_URI))
                        g.add((ref_comp_URI, glycan.has_evidence, ref_ms_URI))
                        g.add((ref_ms_URI, glycan.ms1_verified, Literal(ref_ms1_list[index])))
                        g.add((ref_ms_URI, glycan.ms2_verified, Literal(ref_ms2_list[index])))

            #ce gu, c6
            if is_not_empty_array(row[5]):
                entry = row[5]
                content = entry[2:len(entry)-2]
                list_gu = content.split("], [Glucose")
                profile_id = 'NA'
                for item in list_gu:
                    list = item.split(":")

                    gu = list[1].split(",")[0]
                    area = list[2].split(",")[0]
                    number = list[3].split(",")[0] # could be "null"
                    report = list[4].split(",")[0]
                    report_id = list[5].split(",")[0]
                    sample = list[6].split(",")[0]
                    if len(list) > 8:
                        sample_id = list[7].split(",")[0]
                        profile_id = list[9].split(",")[0].strip()
                    else:
                        sample_id = list[7]

                    if profile_id == 'NA':
                        profile_id = generate_uuid()

                    ref_comp_URI = URIRef(root_prefix.referencecompound + '/' + glycobase_id + '-' + profile_id)

                    g.add((ref_comp_URI, RDF.type, glycan.referenced_compound))
                    g.add((ref_comp_URI, glycan.has_glycan, structure_URI))
                    g.add((ref_comp_URI, glycan.is_from_report, Literal(report)))
                    add_source(g,sample,sample_id,ref_comp_URI)
                    add_source_protein(g,sample,sample_id,structure_URI)

                    # reference_URI = URIRef(root_prefix.reference + '/' +report_id)
                    # g.add((ref_comp_URI, glycan.published_in, reference_URI))
                    # g.add((reference_URI, dc.title, Literal(report)))
                    # g.add((reference_URI, bibo.shortTitle, Literal(sample)))

                    evidence_ce_URI = URIRef(root_prefix.ce + '/' + profile_id)
                    g.add((ref_comp_URI, glycan.is_from_profile, evidence_ce_URI))
                    g.add((evidence_ce_URI, RDF.type, glycan.evidence_ce))
                    g.add((evidence_ce_URI, glycan.has_label, Literal("APTS")))
                    g.add((evidence_ce_URI, glycan.contributed_by, Literal("GlycoBase")))

                    ce_peak_URI = URIRef(root_prefix.ce_peak + '/' + glycobase_id+'-'+profile_id)
                    g.add((evidence_ce_URI, glycan.has_ce_peak, ce_peak_URI))
                    g.add((ref_comp_URI, glycan.has_ce_peak, ce_peak_URI))
                    g.add((structure_URI, glycan.has_ce_peak, ce_peak_URI))
                    g.add((ce_peak_URI, glycan.has_glucose_unit, Literal(gu)))

                    if area != 'null':
                        g.add((ce_peak_URI, glycan.has_peak_area, Literal(area)))
                    if number != 'null':
                        g.add((ce_peak_URI,glycan.has_peak_number,Literal(number)))

            #rpuplc gu, c7
            if is_not_empty_array(row[6]):
                entry = row[6]
                content = entry[2:len(entry)-2]
                list_gu = content.split("], [Glucose")
                profile_id = 'NA'
                result = []
                for item in list_gu:
                    list = item.split(":")

                    gu = list[1].split(",")[0]
                    area = list[2].split(",")[0]
                    number = list[3].split(",")[0] # could be "null"
                    report = list[4].split(",")[0]
                    report_id = list[5].split(",")[0]
                    sample = list[6].split(",")[0]
                    if len(list) > 8:
                        sample_id = list[7].split(",")[0]
                        profile_id = list[9].split(",")[0].strip()
                    else:
                        sample_id = list[7]

                    # # print gu + '\t' + area + '\t'+ number +'\t' + report + '\t'+report_id + '\t' + sample + '\t'+ sample_id + '\t' + profile_id
                    if profile_id == 'NA':
                        profile_id = generate_uuid()

                    ref_comp_URI = URIRef(root_prefix.referencecompound + '/' + glycobase_id + '-' + profile_id)

                    g.add((ref_comp_URI, RDF.type, glycan.referenced_compound))
                    g.add((ref_comp_URI, glycan.has_glycan, structure_URI))
                    g.add((ref_comp_URI, glycan.is_from_report, Literal(report)))
                    add_source(g,sample,sample_id,ref_comp_URI)
                    add_source_protein(g,sample,sample_id,structure_URI)

                    evidence_rpuplc_URI = URIRef(root_prefix.rpuplc + '/' + profile_id)
                    g.add((ref_comp_URI, glycan.is_from_profile, evidence_rpuplc_URI))
                    g.add((evidence_rpuplc_URI, RDF.type, glycan.evidence_rpuplc))
                    g.add((evidence_rpuplc_URI, glycan.has_label, Literal("2-AB")))
                    g.add((evidence_rpuplc_URI, glycan.contributed_by, Literal("GlycoBase")))

                    lc_peak_URI = URIRef(root_prefix.lc_peak + '/' + glycobase_id+'-'+profile_id)
                    g.add((evidence_rpuplc_URI, glycan.has_lc_chromatogram_peak, lc_peak_URI))
                    g.add((ref_comp_URI, glycan.has_lc_chromatogram_peak, lc_peak_URI))
                    g.add((structure_URI,glycan.has_lc_chromatogram_peak, lc_peak_URI))
                    g.add((lc_peak_URI, glycan.has_arabinose_unit, Literal(gu)))

                    if area != 'null':
                        g.add((lc_peak_URI, glycan.has_peak_area, Literal(area)))
                    if number != 'null':
                        g.add((lc_peak_URI,glycan.has_peak_number,Literal(number)))

            #digestion_children, c11
            if is_not_empty_array(row[10]):

                process_digest(row[10],g,'child',glycobase_id)

            #digestion_parent, c12
            if is_not_empty_array(row[11]):
                process_digest(row[11],g,'parent',glycobase_id)

            i += 1

        stop = timeit.default_timer()
        print '\nprocessed ' + str(i) + ' rows\n'

        print '\n----------------------\nused ' + str(stop-start) + ' time\n------------------------\n'


        # print g.serialize(format = 'turtle')
        return g.serialize(format = 'turtle')

# demo_file = '/home/sophie/Desktop/demo.csv'
# demo30 = '/home/sophie/Dropbox/MQU/demo1030.csv'
# demo477 = '/home/sophie/Desktop/demo477.csv' # with literature info
# full_file = '/home/sophie/Desktop/glycobase_spread.txt'
# macfile = '/Users/Sophie/Dropbox/MQU/1110/glycobase_spread.txt'
#
# tomac = '/Users/Sophie/Desktop/1111.ttl'
# tofile = '/home/sophie/Desktop/glycobase1111.ttl'
# f = open(tofile,'w')
# f.write(csv2ttl(full_file))
# f.close()
# # print csv2ttl(full_file)

full_file = '/home/sophie/Dropbox/MQU/glycobase_spread.txt'
# macfile = '/Users/Sophie/Dropbox/MQU/1110/glycobase_spread.txt'

# tomac = '/Users/Sophie/Desktop/1111.ttl'
tofile = '/home/sophie/Desktop/1111.ttl'
f = open(tofile,'w')
f.write(csv2ttl(full_file))
f.close()





from rdflib import URIRef, Literal
from rdflib.namespace import RDF, DCTERMS
from rdflib import Graph
from rdflib import Namespace
import uuid
import sys
import csv
maxInt = sys.maxsize
decrement = True

while decrement:
    # decrease the maxInt value by factor 10
    # as long as the OverflowError occurs.
    decrement = False
    try:
        csv.field_size_limit(maxInt)
    except OverflowError:
        maxInt = int(maxInt/10)
        decrement = True

# if the String inside [ ] is not empty, return True
def is_not_empty_array(str):
    if str[1:len(str)-1]:
        return True
    else:
        return False

# extract content inside [ ], make them into a list of strings
def str_to_list(str):
    content = str[1:len(str)-1]
    list_of_str = content.split(",")
    return list_of_str

# extract digestion info, this entry has two layer of [], in the format of [[],[],...], return each digrest child/parent entry as an item in list
def digest_list(str):
    list = str[2:len(str)-3]
    list_of_digest = list.split("],[")
    return list_of_digest

def remove_last_comma(str):
    new_str = str[0:len(str)-1]
    return new_str

def digest_item_per_unit(item):
    cformat = item.replace("Technique:","|").replace("name:","|").replace("enzymes:","|").\
                            replace("retention (GU): ","|").replace("profile ID: ","|").replace("profile name:","|").\
                            replace("profile instrument: ","|").replace("profile dextran standard: ","|")
    list = cformat.split("|")
    return list

def generate_uuid():
    uid_str_full = uuid.uuid4().urn
    uid_str = uid_str_full[9:]
    return uid_str

def csv2ttl(csvfile):
    # -----------------------  set NameSpace   ----------------------------
    # Namespaces in glycan.owl that are missed in rdflib, put here in case of usage later
    bibo = Namespace('http://purl.org/ontology/bibo/')
    glycan = Namespace('http://purl.jp/bio/12/glyco/glycan/')
    dcterms = Namespace('http://purl.org/dc/terms/')
    dc = Namespace('http://purl.org/dc/elements/1.1/')

    # Namespaces for glycobase project
    root_prefix = Namespace('http://rdf.glycobase.org/')
    resource_prefix = Namespace('https://glycobase.nibrt.ie/glycobase/show_glycan.action?glycanSequenceId=')

    # ----------------------  start to construct a graph for input data -------------------
    g = Graph()
    g.bind('glycan',glycan)
    g.bind('bibo',bibo)
    g.bind('dcterms',dcterms)
    g.bind('dc',dc)

    # system generated URIs for inner reference, +1 after every usage
    ref_comp_id = 1     # referenced_compound index
    digest_id = 1       # source index
    lc_peak_id = 1      # lc peak index
    ce_peak_id = 1      # ce peak index
    evidence_id = 4000  # for records in glycobase without a profile id
    reaction_id = 3000

    def add_source(graph,sample_title):
        taxon = ''
        tissue =''
        id = '100'
        if sample_title == 'Cow':
            taxon = 'Bos taurus'
            tissue = 'Anatomy'
            id = '1'
        elif sample_title == 'Human Serum':
            taxon = 'Homo sapiens'
            tissue = 'Anatomy'
            id = '2'
        elif sample_title == 'Pig':
            taxon = 'Sus scrofa'
            tissue = 'Milk'
            id = '3'
        elif sample_title == 'Sheep':
            taxon = 'Ovis aries'
            tissue = 'Anatomy'
            id = '4'
        elif sample_title == 'Horse':
            taxon = 'Equus caballus'
            tissue = 'Milk'
            id = '5'
        elif sample_title == 'Goat':
            taxon = 'Capra aegagrus hircus'
            tissue = 'Milk'
            id = '6'
        elif sample_title == 'Dromedary Camel':
            taxon = 'Camelus dromedarius'
            tissue = 'Milk'
            id = '7'
        elif (sample_title == 'CHO B' or sample_title == 'CHO A'):
            taxon = 'Cricetulus griseus'
            tissue = 'Anatomy'
            id = '8'
        if id != '100':
            source_URI = URIRef(root_prefix.source + '/' + id)
            graph.add((source_URI, RDF.type, glycan.source_nature))
            graph.add((source_URI, glycan.has_taxon, taxon))
            graph.add((source_URI, glycan.has_tissue, tissue))
        return graph

    def get_sourceID(sample_title):
        id = '100'
        if sample_title == 'Cow':
            id = '1'
        elif sample_title == 'Human Serum':
            id = '2'
        elif sample_title == 'Pig':
            id = '3'
        elif sample_title == 'Sheep':
            id = '4'
        elif sample_title == 'Horse':
            id = '5'
        elif sample_title == 'Goat':
            id = '6'
        elif sample_title == 'Dromedary Camel':
            id = '7'
        elif (sample_title == 'CHO B' or sample_title == 'CHO A'):
            id = '8'
        return id

    def add_reference_compound(graph, glycan_structure_URI, evidence_URI, ref_comp_id,report_id,source_id):
        ref_comp_URI = URIRef(root_prefix.referencecompound + '/' + str(ref_comp_id))
        reference_URI = URIRef(root_prefix.reference + '/' + report_id)
        graph.add((ref_comp_URI, RDF.type, glycan.referenced_compound))
        graph.add((ref_comp_URI, glycan.has_glycan, glycan_structure_URI))
        if source_id != '100':
            source_URI = URIRef(root_prefix.source + '/'+ source_id)
            graph.add((ref_comp_URI, glycan.is_from_source,source_URI))
        graph.add((ref_comp_URI, glycan.published_in, reference_URI))
        graph.add((ref_comp_URI, glycan.has_evidence, evidence_URI))
        return graph

    def check_evidence_URI(type_str,profile_id):
        result_URI = ''
        if type_str == "UPLC":
            result_URI = URIRef(root_prefix.uplc + '/' + profile_id)
        elif type_str == 'HPLC':
            result_URI = URIRef(root_prefix.hplc + '/' + profile_id)
        elif type_str == 'CE':
            result_URI = URIRef(root_prefix.ce + '/' + profile_id)
        elif type_str == 'RPUPLC':
            result_URI = URIRef(root_prefix.rpuplc + '/' + profile_id)
        return result_URI

    def digest_evidence_type(type_str):
        type = ''
        if type_str == "UPLC":
            type = 'glycan.evidence_uplc'
        elif type_str == 'HPLC':
            type = 'glycan.evidence_hplc'
        elif type_str == 'CE':
            type = 'glycan.evidence_ce'
        elif type_str == 'RPUPLC':
            type = 'glycan.evidence_rpuplc'
        return Literal(type)

    with open(csvfile, 'rb') as csvfile:
        next(csvfile) # skip the header in the first row
        infile = csv.reader(csvfile, delimiter='\t', quotechar='|')
        for row in infile:
            profileID_str_array =[]
            uoxf = Literal(row[0])
            # print uoxf
            glycobaseID = Literal(row[1])

            # define URIs using glycobaseID for outer linkage
            # ref_comp_uuid = generate_uuid()
            structure_URI = URIRef(root_prefix.structure + '/' + glycobaseID)
            resource_URI = URIRef(resource_prefix + glycobaseID)
            ct_sequence_URI = URIRef(root_prefix.structure + '/' + glycobaseID + "/ct")        # fit unicarbkb

            # start a statement for structure class
            g.add((structure_URI, RDF.type, glycan.saccharide))
            g.add((structure_URI, glycan.glycosequence, ct_sequence_URI))
            g.add((structure_URI, glycan.has_glycosequence, ct_sequence_URI))
            g.add((structure_URI, glycan.has_resource_entry, resource_URI))
            g.add((structure_URI, glycan.has_uoxf, uoxf))

            # add glycoct sequence
            ct_content = row[2].replace(",","\n")
            ct_sequence = Literal(ct_content)
            g.add((ct_sequence_URI, RDF.type, glycan.glycosequence))
            g.add((ct_sequence_URI, glycan.has_sequence, ct_sequence))
            g.add((ct_sequence_URI, glycan.in_carbohydrate_format, glycan.carbohydrate_format_glycoct))

            # add resource_entry
            g.add((resource_URI, RDF.type, glycan.resource_entry))
            g.add((resource_URI, glycan.in_glycan_database, glycan.database_glycobase))
            g.add((resource_URI, DCTERMS.identifier,glycobaseID))

            #------------------------------reference entry --------------------------------
            # #report_id, c12
            if is_not_empty_array(row[11]):
                reportID_str_array =  str_to_list(row[11])
            #report title, c11
            # e.g. published glycans /CE Database /NIBRT Public Collection
            if is_not_empty_array(row[10]):
                for item in str_to_list(row[10]):
                    report_title = Literal(item)
                    # add report title to graph
                    report_id = Literal(reportID_str_array[0].lstrip())
                    reportID_str_array.pop(0)

                    reference_URI = URIRef(root_prefix.reference + '/' + report_id)
                    g.add((reference_URI, RDF.type, bibo.Article))
                    g.add((reference_URI, dc.title, report_title))
                    # print "report_title: " + item

            if is_not_empty_array(row[11]):
                reportID_str_array =  str_to_list(row[11])
            #sample_title, c13
            # e.g. cow /sheep /Professor Rudd NIBRT Data /Human Serum
            if is_not_empty_array(row[12]):
                sample_title_array = str_to_list(row[12])
                for item in sample_title_array:
                    sample_title = Literal(item)
                    # add sample_title to graph
                    report_id = Literal(reportID_str_array[0].lstrip())
                    reportID_str_array.pop(0)

                    reference_URI = URIRef(root_prefix.reference + '/' + report_id)
                    g.add((reference_URI, RDF.type, bibo.Article))
                    g.add((reference_URI, bibo.shortTitle, sample_title))

                    source_id = get_sourceID(sample_title)
                    if source_id != '100':
                        add_source(g,sample_title)
                    # print "sample_title: " + item
            # #sample_id, c14
            # #profile_title, c15

            # -------------------------  read in evidence -----------------------------------
            #profile_id, c16
            if is_not_empty_array(row[15]):
                profileID_str_array =  str_to_list(row[15])

            # #report_id, c12
            if is_not_empty_array(row[11]):
                reportID_str_array =  str_to_list(row[11])
            # uplc gu, column 4
            if is_not_empty_array(row[3]):
                # add uplc_gu to graph
                for item in str_to_list(row[3]):
                    uplc_gu = Literal(item)

                    if profileID_str_array[0].lstrip() == 'NA':
                        tmp_id = Literal(evidence_id)
                        evidence_id += 1
                    else:
                        tmp_id = Literal(profileID_str_array[0].lstrip())
                    profileID_str_array.pop(0)

                    report_id = Literal(reportID_str_array[0].lstrip())
                    reportID_str_array.pop(0)
                    evidence_uplc_URI = URIRef(root_prefix.uplc + '/' + tmp_id)
                    g.add((structure_URI, glycan.has_evidence, evidence_uplc_URI))
                    g.add((evidence_uplc_URI, RDF.type, glycan.evidence_uplc))
                    lc_peak_URI = URIRef(root_prefix.lc_peak + '/' + str(lc_peak_id))
                    g.add((evidence_uplc_URI, glycan.has_lc_chromatogram_peak, lc_peak_URI))
                    g.add((lc_peak_URI, glycan.has_glucose_unit, uplc_gu))
                    lc_peak_id += 1
                    source_index = get_sourceID(sample_title_array[0])
                    sample_title_array.pop(0)
                    add_reference_compound(g,structure_URI,evidence_uplc_URI,ref_comp_id,report_id,source_index)
                    ref_comp_id += 1
                    # print "uplc: " + item

            #hplc gu, c5
            if is_not_empty_array(row[4]):
                # add hplc_gu to graph
                for item in str_to_list(row[4]):
                    hplc_gu = Literal(item)

                    if profileID_str_array[0].lstrip() == 'NA':
                        tmp_id = Literal(evidence_id)
                        evidence_id += 1
                    else:
                        tmp_id = Literal(profileID_str_array[0].lstrip())
                    profileID_str_array.pop(0)
                    report_id = Literal(reportID_str_array[0].lstrip())
                    reportID_str_array.pop(0)
                    evidence_hplc_URI = URIRef(root_prefix.hplc + '/' + tmp_id)
                    g.add((structure_URI, glycan.has_evidence, evidence_hplc_URI))
                    g.add((evidence_hplc_URI, RDF.type, glycan.evidence_hplc))
                    lc_peak_URI = URIRef(root_prefix.lc_peak + '/' + str(lc_peak_id))
                    g.add((evidence_hplc_URI, glycan.has_lc_chromatogram_peak, lc_peak_URI))
                    g.add((lc_peak_URI, glycan.has_glucose_unit, hplc_gu))
                    lc_peak_id += 1
                    source_index = get_sourceID(sample_title_array[0])
                    sample_title_array.pop(0)
                    add_reference_compound(g,structure_URI,evidence_hplc_URI,ref_comp_id,report_id,source_index)
                    ref_comp_id += 1
                    # print "hplc: " + item

            #ce gu, c6
            if is_not_empty_array(row[5]):
                # add ce_gu to graph
                for item in str_to_list(row[5]):
                    ce_gu = Literal(item)

                    if profileID_str_array[0].lstrip() == 'NA':
                        tmp_id = Literal(evidence_id)
                        evidence_id += 1
                    else:
                        tmp_id = Literal(profileID_str_array[0].lstrip())
                    profileID_str_array.pop(0)
                    report_id = Literal(reportID_str_array[0].lstrip())
                    reportID_str_array.pop(0)
                    evidence_ce_URI = URIRef(root_prefix.ce + '/' + tmp_id)
                    g.add((structure_URI, glycan.has_evidence, evidence_ce_URI))
                    g.add((evidence_ce_URI, RDF.type, glycan.evidence_ce))
                    ce_peak_URI = URIRef(root_prefix.ce_peak + '/' + str(ce_peak_id))
                    g.add((evidence_ce_URI, glycan.has_ce_peak, ce_peak_URI))
                    g.add((ce_peak_URI, glycan.has_glucose_unit, ce_gu))
                    ce_peak_id += 1
                    source_index = get_sourceID(sample_title_array[0])
                    sample_title_array.pop(0)
                    add_reference_compound(g,structure_URI,evidence_ce_URI,ref_comp_id,report_id,source_index)
                    ref_comp_id += 1
                    # print "ce: " + item

            #rpuplc gu, c7
            if is_not_empty_array(row[6]):
                # add rpuplc_gu to graph
                for item in str_to_list(row[6]):
                    rpuplc_gu = Literal(item)

                    if profileID_str_array[0].lstrip() == 'NA':
                        tmp_id = Literal(evidence_id)
                        evidence_id += 1
                    else:
                        tmp_id = Literal(profileID_str_array[0].lstrip())
                    profileID_str_array.pop(0)
                    report_id = Literal(reportID_str_array[0].lstrip())
                    reportID_str_array.pop(0)
                    evidence_rpuplc_URI = URIRef(root_prefix.rpuplc + '/' + tmp_id)
                    g.add((structure_URI, glycan.has_evidence, evidence_rpuplc_URI))
                    g.add((evidence_rpuplc_URI, RDF.type, glycan.evidence_rpuplc))
                    lc_peak_URI = URIRef(root_prefix.lc_peak + '/' + str(lc_peak_id))
                    g.add((evidence_rpuplc_URI, glycan.has_lc_chromatogram_peak, lc_peak_URI))
                    g.add((lc_peak_URI, glycan.has_arabinose_unit, rpuplc_gu))
                    lc_peak_id += 1
                    source_index = get_sourceID(sample_title_array[0])
                    sample_title_array.pop(0)
                    add_reference_compound(g,structure_URI,evidence_rpuplc_URI,ref_comp_id,report_id,source_index)
                    ref_comp_id += 1
                    # print "rpuplc: " + item

            # ----------------------------- what is the relationship between source and gu ??---------------------------
            # #taxonomy, c8
            # # taxonomy and tissue have 1 to 1 relationship, not sure about the relationship to gu value
            # if is_not_empty_array(row[7]):
            #     for item in str_to_list(row[7]):
            #         taxonomy = Literal(item)
            #         # add taxonomy to graph
            #         add_source(g, source_id)
            #         source_URI = URIRef(root_prefix.source + '/' + str(source_id))
            #         g.add((source_URI, glycan.has_taxon, taxonomy))
            #         source_id += 1
            #         # print "taxonomy: " + item
            #
            # #tissue, c9
            # if is_not_empty_array(row[8]):
            #     source_id = source_id - len(str_to_list(row[7]))
            #     for item in str_to_list(row[8]):
            #         tissue = Literal(item)
            #         # add tissue to graph
            #         add_source(g, source_id)
            #         source_URI = URIRef(root_prefix.source + '/' + str(source_id))
            #         g.add((source_URI, glycan.has_tissue, tissue))
            #         source_id += 1
            #         # print "tissue: " + item
            #
            # #disease, c10
            # # !!!!! disease is not 1 to 1 for taxon-tissue, relationship need to decide later !!!!!!
            # if is_not_empty_array(row[9]):
            #     for item in str_to_list(row[9]):
            #         disease = Literal(item)
            #         # add disease to graph
            #         add_source(g, source_id)
            #         source_URI = URIRef(root_prefix.source + '/' + str(source_id))
            #         g.add((source_URI, glycan.has_disease, disease))
            #         # print "disease: " + item

            #digestion_children, c17
            if is_not_empty_array(row[16]):
                for item in digest_list(row[16]):
                    array_in_item = digest_item_per_unit(item)

                    digest_child_id = remove_last_comma(array_in_item[0])
                    child_evidence_type = remove_last_comma(array_in_item[1])
                    child_uoxf = remove_last_comma(array_in_item[2])
                    child_enzyme = remove_last_comma(array_in_item[3])
                    child_retention = remove_last_comma(array_in_item[4])
                    child_profileID = remove_last_comma(array_in_item[5]).split(",")[0]

                    child_profile_instrument = remove_last_comma(array_in_item[7])


                    childID = Literal(digest_child_id)
                    childUOXF = Literal(child_uoxf)
                    childEnzyme = Literal(child_enzyme)
                    childGU = Literal(child_retention)
                    childPrflInstr = Literal(child_profile_instrument)


                    # add digest child to current structure with "has_compound"
                    child_structure_URI = URIRef(root_prefix.structure + '/' + childID)

                    if child_profileID == 'NA':
                        child_profileID = str(reaction_id)
                        reaction_id += 1

                    g.add((child_structure_URI, RDF.type, glycan.saccharide))              # just like a new structure statement
                    child_evidence_URI = check_evidence_URI(child_evidence_type,child_profileID)
                    g.add((child_structure_URI, glycan.has_evidence, child_evidence_URI))
                    g.add((child_evidence_URI, RDF.type, digest_evidence_type(child_evidence_type)))
                    digest_URI = URIRef(root_prefix.digest + '/'+ Literal(digest_id))
                    digest_id += 1
                    if child_evidence_type == 'CE':
                        g.add((child_evidence_URI,glycan.has_ce_digest_chromatogram,digest_URI))
                        ce_peak_URI = URIRef(root_prefix.ce_peak + '/' + str(ce_peak_id))
                        g.add((child_evidence_URI, glycan.has_ce_peak, ce_peak_URI))
                        g.add((ce_peak_URI, glycan.has_glucose_unit, childGU))
                        ce_peak_id += 1
                    else:
                        g.add((child_evidence_URI, glycan.has_lc_digest_chromatogram, digest_URI))
                        lc_peak_URI = URIRef(root_prefix.lc_peak + '/' + str(lc_peak_id))
                        g.add((child_evidence_URI, glycan.has_lc_chromatogram_peak, lc_peak_URI))
                        g.add((lc_peak_URI, glycan.has_glucose_unit, childGU))
                        lc_peak_id += 1
                    g.add((digest_URI, glycan.has_exglycosidase_treatment, childEnzyme))
                    g.add((digest_URI, glycan.used_instrument,childPrflInstr))
                    if len(array_in_item)>8:
                        child_pf_dextran_sd = remove_last_comma(array_in_item[8])
                        childPrflDextran = Literal(child_pf_dextran_sd)
                        g.add((digest_URI, glycan.dextran_standard, childPrflDextran))


                    add_reference_compound(g,child_structure_URI,child_evidence_URI,ref_comp_id,child_profileID,'100')
                    ref_comp_URI = URIRef(root_prefix.referencecompound + '/' + str(ref_comp_id))
                    reaction_URI = URIRef(root_prefix.reaction + '/' + child_profileID)
                    g.add((ref_comp_URI, glycan.has_reaction, reaction_URI))
                    g.add((reaction_URI, glycan.catalyzed_by, childEnzyme))
                    g.add((reaction_URI, glycan.has_substrate, structure_URI))
                    ref_comp_id += 1
                    child_resource_URI = URIRef(resource_prefix + childID)
                    g.add((child_structure_URI, glycan.has_resource_entry, child_resource_URI))
                    g.add((child_resource_URI, RDF.type, glycan.resource_entry))
                    g.add((child_resource_URI, glycan.in_glycan_database, glycan.database_glycobase))
                    g.add((child_resource_URI, DCTERMS.identifier,childID))
                    g.add((child_structure_URI, glycan.has_uoxf, childUOXF))

            #digestion_parent, c18
            if is_not_empty_array(row[17]):
                for item in digest_list(row[17]):
                    array_in_item = digest_item_per_unit(item)
                    digest_parent_id = remove_last_comma(array_in_item[0])
                    parent_evidence_type = remove_last_comma(array_in_item[1])
                    parent_uoxf = remove_last_comma(array_in_item[2])

                    parentID = Literal(digest_parent_id)
                    parentUOXF = Literal(parent_uoxf)


                    if len(array_in_item) > 3:
                        parent_enzyme = remove_last_comma(array_in_item[3])
                        parentEnzyme = Literal(parent_enzyme)
                    if len(array_in_item) > 4:
                        parent_retention = remove_last_comma(array_in_item[4])
                        parentGU = Literal(parent_retention)
                        parent_profileID = remove_last_comma(array_in_item[5]).split(",")[0]
                        parent_profile_instrument = remove_last_comma(array_in_item[7])
                        parentPrflInstr = Literal(parent_profile_instrument)

                        # add digest parent to current structure with "has_compound"
                        parent_structure_URI = URIRef(root_prefix.structure + '/' + parentID)

                        if parent_profileID == 'NA':
                            parent_profileID = str(reaction_id)
                            reaction_id += 1

                        # g.add((structure_URI, glycan.has_compound, parent_structure_URI))
                        g.add((parent_structure_URI, RDF.type, glycan.saccharide))              # just like a new structure statement
                        parent_evidence_URI = check_evidence_URI(parent_evidence_type,parent_profileID)
                        g.add((parent_structure_URI, glycan.has_evidence, parent_evidence_URI))
                        g.add((parent_evidence_URI, RDF.type, digest_evidence_type(parent_evidence_type)))
                        digest_URI = URIRef(root_prefix.digest + '/'+ Literal(digest_id))
                        digest_id += 1
                        if parent_evidence_type == 'CE':
                            g.add((parent_evidence_URI,glycan.has_ce_digest_chromatogram,digest_URI))
                            ce_peak_URI = URIRef(root_prefix.ce_peak + '/' + str(ce_peak_id))
                            g.add((parent_evidence_URI, glycan.has_ce_peak, ce_peak_URI))
                            g.add((ce_peak_URI, glycan.has_glucose_unit, parentGU))
                            ce_peak_id += 1
                        else:
                            g.add((parent_evidence_URI, glycan.has_lc_digest_chromatogram, digest_URI))
                            lc_peak_URI = URIRef(root_prefix.lc_peak + '/' + str(lc_peak_id))
                            g.add((parent_evidence_URI, glycan.has_lc_chromatogram_peak, lc_peak_URI))
                            g.add((lc_peak_URI, glycan.has_glucose_unit, parentGU))
                            lc_peak_id += 1
                        g.add((digest_URI, glycan.has_exglycosidase_treatment, parentEnzyme))
                        g.add((digest_URI, glycan.used_instrument,parentPrflInstr))
                        if len(array_in_item) >8:
                            parent_pf_dextran_sd = remove_last_comma(array_in_item[8])
                            parentPrflDextran = Literal(parent_pf_dextran_sd)
                            g.add((digest_URI, glycan.dextran_standard, parentPrflDextran))


                        add_reference_compound(g,parent_structure_URI,parent_evidence_URI,ref_comp_id,parent_profileID,'100')
                        ref_comp_URI = URIRef(root_prefix.referencecompound + '/' + str(ref_comp_id))
                        reaction_URI = URIRef(root_prefix.reaction + '/' + parent_profileID)
                        g.add((ref_comp_URI, glycan.has_reaction, reaction_URI))
                        g.add((reaction_URI, glycan.has_exglycosidase, parentEnzyme))
                        g.add((reaction_URI, glycan.has_product, structure_URI))
                        ref_comp_id += 1
                        parent_resource_URI = URIRef(resource_prefix + parentID)
                        g.add((parent_structure_URI, glycan.has_resource_entry, parent_resource_URI))
                        g.add((parent_resource_URI, RDF.type, glycan.resource_entry))
                        g.add((parent_resource_URI, glycan.in_glycan_database, glycan.database_glycobase))
                        g.add((parent_resource_URI, DCTERMS.identifier,parentID))
                        g.add((parent_structure_URI, glycan.has_uoxf, parentUOXF))

        # print g.serialize(format = 'turtle')
        return g.serialize(format = 'turtle')

demo_file = '/home/sophie/Desktop/demo.csv'
demo30 = '/home/sophie/Dropbox/MQU/demo1030.csv'
full_file = '/home/sophie/Desktop/glycobase_spread.csv'
macfile = '/Users/Sophie/Dropbox/MQU/demo1030.csv'

tofile = '/home/sophie/Desktop/glycobase.ttl'
f = open(tofile,'w')
f.write(csv2ttl(full_file))
f.close()
# print csv2ttl(full_file)

from rdflib import URIRef, Literal
from rdflib.namespace import RDF, DCTERMS, XSD
from rdflib import Graph
from prefix import glycan, root_prefix, bibo,dc,dcterms,owl
from digestion import process_digest
from literature import read_literature, read_papers
from source import add_source, add_source_protein
# from unicornRestMass import matchGlycoCT
from searchGlytoucan import extractMass
import uuid
import sys
import csv
import timeit
import math
start = timeit.default_timer()
maxInt = sys.maxsize
decrement = True

while decrement:
    # decrease the maxInt value by factor 10
    # as long as the OverflowError occurs.
    decrement = False
    try:
        csv.field_size_limit(maxInt)
    except OverflowError:
        maxInt = int(maxInt/10)
        decrement = True

# if the String inside [ ] is not empty, return True
def is_not_empty_array(str):
    if str[1:len(str)-1]:
        return True
    else:
        return False

# extract content inside [ ], make them into a list of strings
def str_to_list(str):
    content = str[1:len(str)-1]
    list_of_str = content.split(",")
    return list_of_str

def remove_last_comma(str):
    new_str = str[0:len(str)-1]
    return new_str

def generate_uuid():
    uid_str_full = uuid.uuid4().urn
    uid_str = uid_str_full[9:]
    return uid_str

def csv2ttl(csvfile):

    # ----------------------  start to construct a graph for input data -------------------
    g = Graph()
    g.bind('glycan',glycan)
    g.bind('bibo',bibo)
    g.bind('dcterms',dcterms)
    g.bind('dc',dc)
    g.bind('owl', owl)

    ref_gu_list = []
    ref_ms2_list = []
    ref_ms1_list = []
    ref_pubmedID_list =[]

    # construct literature graph from literature files
    g = read_papers('/home/sophie/Dropbox/MQU/pubmed_result.csv',g)

    with open(csvfile, 'rb') as csvfile:
        next(csvfile) # skip the header in the first row
        infile = csv.reader(csvfile, delimiter='\t', quotechar='|')
        i = 0
        for row in infile:
            print '\n start ' + str(i) + " row\n"
            uoxf = row[0]
            print uoxf
            glycobase_id = row[1]

            # define URIs using glycobaseID for outer linkage
            structure_URI = URIRef(root_prefix.structure + '/' + glycobase_id)
            ct_sequence_URI = URIRef(root_prefix.structure + '/' + glycobase_id + "/ct")        # fit unicarbkb

            # start a statement for structure class
            g.add((structure_URI, RDF.type, glycan.saccharide))
            g.add((structure_URI, glycan.glycosequence, ct_sequence_URI))
            g.add((structure_URI, glycan.has_glycosequence, ct_sequence_URI))
            g.add((structure_URI, glycan.has_uoxf, Literal(uoxf)))
            g.add((structure_URI, glycan.has_glycobase_id,Literal(glycobase_id,datatype=XSD.integer)))

            # add glycoct sequence
            ct_content = row[2].replace(",","\n")
            # add mass from rest query of unicarbkb
            mass=extractMass(glycobase_id)
            if mass != "not found":
                g.add((structure_URI, glycan.has_monoisotopic_molecular_weight, Literal(mass,datatype=XSD.float)))
            else:
                print glycobase_id, " need mass to be added manually later"
            # if uoxf == 'F(6)A4G(4)4S(3,3,3,3)4' or uoxf == 'F(6)A4G(4)4S(3,3,3,6)4':
            #     g.add((structure_URI, glycan.has_monoisotopic_molecular_weight, Literal(3681.2961,datatype=XSD.float)))
            #     g.add((structure_URI, glycan.has_average_molecular_weight, Literal(3681.2961,datatype=XSD.float)))
            # elif uoxf == 'F(6)A4G(4)4S(6,6)2' or uoxf == 'F(6)A4G(4)4S(3,3)2':#row 145
            #     g.add((structure_URI, glycan.has_monoisotopic_molecular_weight, Literal(3099.1053,datatype=XSD.float)))
            #     g.add((structure_URI, glycan.has_average_molecular_weight, Literal(3099.1053,datatype=XSD.float)))
            # else:
            #     mass_list = matchGlycoCT(ct_content)
            #     # print mass_list
            #     if mass_list != "NA":
            #         g.add((structure_URI, glycan.has_monoisotopic_molecular_weight, Literal(mass_list[0],datatype=XSD.float)))
            #         g.add((structure_URI, glycan.has_average_molecular_weight, Literal(mass_list[1],datatype=XSD.float)))
            #     else:
            #         print uoxf, " doesn't have mass from unicarbkb"

            ct_sequence = Literal(ct_content)
            g.add((ct_sequence_URI, RDF.type, glycan.glycosequence))
            g.add((ct_sequence_URI, glycan.has_sequence, ct_sequence))
            g.add((ct_sequence_URI, glycan.in_carbohydrate_format, glycan.carbohydrate_format_glycoct))

            # add resource_entry
            resource_URI = URIRef(root_prefix + glycobase_id)
            g.add((structure_URI, glycan.has_resource_entry, resource_URI))
            g.add((resource_URI, RDF.type, glycan.resource_entry))
            g.add((resource_URI, DCTERMS.identifier,Literal(glycobase_id)))
            g.add((resource_URI, glycan.in_glycan_database, glycan.database_glycostore))

            # -------------- add paper reference------------------
            #literature, c13
            if is_not_empty_array(row[12]):
                literature_content = read_literature(row[12])
                ref_gu_list = literature_content[0]
                ref_ms2_list = literature_content[1]
                ref_ms1_list = literature_content[2]
                ref_pubmedID_list = literature_content[3]

            # -------------------------  read in evidence -----------------------------------
            def add_gu_from_main(g,cell_content,glycobase_id,structure_URI,peak_predicate,has_predicate,
                                 evidence_predicate, evidence_type,guau_predicate,label):
                content = cell_content[2:len(cell_content)-2]
                list_gu = content.split("], [Glucose")
                profile_id = 'NA'
                for item in list_gu:
                    list = item.split(":")

                    gu = list[1].split(",")[0]
                    if float(gu) != 0:
                        area = list[2].split(",")[0]
                        number = list[3].split(",")[0] # could be "null"
                        report = list[4].split(",")[0]
                        sample = list[6].split(",")[0]
                        if len(list) > 8:
                            sample_id = list[7].split(",")[0]
                            profile_id = list[9].split(",")[0].strip()
                        else:
                            sample_id = list[7]

                        # # print gu + '\t' + area + '\t'+ number +'\t' + report + '\t'+report_id + '\t' + sample + '\t'+ sample_id + '\t' + profile_id
                        if profile_id == 'NA':
                            profile_id = generate_uuid()
                        ref_comp_URI = URIRef(root_prefix.referencecompound + '/' + glycobase_id + '-' + profile_id)

                        g.add((ref_comp_URI, RDF.type, glycan.referenced_compound))
                        g.add((ref_comp_URI, glycan.has_glycan, structure_URI))
                        g.add((ref_comp_URI, glycan.is_from_report, Literal(report)))
                        add_source(g,sample,sample_id,ref_comp_URI)
                        add_source_protein(g,sample,sample_id,structure_URI)

                        # peak_predicate refers to: root_prefix.ce_peak or root_prefix.lc_peak
                        peak_URI = URIRef(peak_predicate + '/' + glycobase_id+'-'+profile_id) # where gu is stored
                        # has_predicate refers to: glycan.has_lc_chromatogram_peak or glycan.has_ce_peak
                        g.add((ref_comp_URI, has_predicate, peak_URI))
                        g.add((structure_URI, has_predicate, peak_URI))
                        # evidence_predicate refers to: root_prefix.rpuplc; root_prefix.ce; root_prefix.hplc; root_prefix.uplc
                        evidence_profile_URI = URIRef(evidence_predicate + '/' + profile_id)
                        g.add((ref_comp_URI,glycan.is_from_profile, evidence_profile_URI))
                        # evidence_type refers to: glycan.evidence_rpuplc; glycan.evidence_uplc; glycan.evidence_hplc; glycan.evidence_ce
                        g.add((evidence_profile_URI, RDF.type, evidence_type))
                        g.add((evidence_profile_URI, has_predicate, peak_URI))
                        # assume all LC-MS profile are labeled with '2-AB', CE labeled with 'APTS'
                        g.add((evidence_profile_URI, glycan.has_label, Literal(label)))
                        g.add((evidence_profile_URI, glycan.contributed_by, Literal("GlycoBase")))
                        # guau_predicate refers to: glycan.has_arabinose_unit or glycan.has_glucose_unit
                        g.add((peak_URI, guau_predicate, Literal(gu,datatype=XSD.float)))
                        if area != 'null':
                            g.add((peak_URI, glycan.has_peak_area, Literal(area)))
                        if number != 'null':
                            g.add((peak_URI,glycan.has_peak_number,Literal(number)))
                return g


            # uplc gu, column 4
            if is_not_empty_array(row[3]):
                entry = row[3]
                # print "\nreading uplc_gu cell....\n"

                add_gu_from_main(g,entry,glycobase_id,structure_URI,root_prefix.lc_peak,glycan.has_lc_chromatogram_peak,
                                 root_prefix.uplc,glycan.evidence_uplc,glycan.has_glucose_unit,"2-AB")


            #hplc gu, c5
            if is_not_empty_array(row[4]):
                entry = row[4]
                # print "\nreading hplc_gu cell....\n"

                # add_gu_from_main(g,entry,glycobase_id,structure_URI,root_prefix.lc_peak,glycan.has_lc_chromatogram_peak,
                #                  root_prefix.hplc,glycan.evidence_hplc,glycan.has_glucose_unit)

                content = entry[2:len(entry)-2]
                list_gu = content.split("], [Glucose")
                profile_id = 'NA'
                for item in list_gu:
                    list = item.split(":")

                    gu = list[1].split(",")[0]
                    if float(gu) != 0:
                        area = list[2].split(",")[0]
                        number = list[3].split(",")[0] # could be "null"
                        report = list[4].split(",")[0]
                        report_id = list[5].split(",")[0]
                        sample = list[6].split(",")[0]
                        if len(list) > 8:
                            sample_id = list[7].split(",")[0]
                            profile_id = list[9].split(",")[0].strip()
                            if profile_id == 'NA':
                                profile_id = generate_uuid()
                        else:
                            sample_id = list[7]
                            profile_id = generate_uuid()

                        ref_comp_URI = URIRef(root_prefix.referencecompound + '/' + glycobase_id + '-' + profile_id)

                        g.add((ref_comp_URI, RDF.type, glycan.referenced_compound))
                        g.add((ref_comp_URI, glycan.has_glycan, structure_URI))
                        g.add((ref_comp_URI, glycan.is_from_report, Literal(report)))
                        add_source(g,sample,sample_id,ref_comp_URI)
                        add_source_protein(g,sample,sample_id,structure_URI)

                        lc_peak_URI = URIRef(root_prefix.lc_peak + '/' + glycobase_id+'-'+profile_id)
                        g.add((ref_comp_URI, glycan.has_lc_chromatogram_peak, lc_peak_URI))
                        g.add((structure_URI, glycan.has_lc_chromatogram_peak, lc_peak_URI))

                        evidence_hplc_URI = URIRef(root_prefix.hplc + '/' + profile_id)
                        g.add((ref_comp_URI,glycan.is_from_profile, evidence_hplc_URI))
                        g.add((evidence_hplc_URI, RDF.type, glycan.evidence_hplc))
                        g.add((evidence_hplc_URI, glycan.has_lc_chromatogram_peak, lc_peak_URI))
                        g.add((evidence_hplc_URI, glycan.has_label, Literal("2-AB")))
                        g.add((evidence_hplc_URI, glycan.contributed_by, Literal("GlycoBase")))


                        g.add((lc_peak_URI, glycan.has_glucose_unit, Literal(gu,datatype=XSD.float)))
                        if area != 'null':
                            g.add((lc_peak_URI, glycan.has_peak_area, Literal(area)))
                        if number != 'null':
                            g.add((lc_peak_URI,glycan.has_peak_number,Literal(number)))

                        def check_ref_gu(evidence_gu,ref_gu_list):
                            i = 0
                            while i < len(ref_gu_list):
                                if ref_gu_list[i].strip() == evidence_gu:
                                    return i
                                i += 1
                            return 100
                        index = check_ref_gu(gu,ref_gu_list)
                        if index != 100:
                            literature_URI = URIRef(root_prefix.literature + '/' +ref_pubmedID_list[index])
                            ref_ms_URI = URIRef(root_prefix.evidence_ms + '/'+ glycobase_id + '-' + profile_id)
                            g.add((ref_comp_URI, glycan.published_in, literature_URI))
                            g.add((ref_comp_URI, glycan.has_evidence, ref_ms_URI))
                            # if ref_ms1_list[index] != 'NA':
                            #     g.add((ref_ms_URI, glycan.ms1_verified, Literal(ref_ms1_list[index])))
                            #     if ref_ms2_list[index] != 'NA':
                            #         g.add((ref_ms_URI, glycan.ms2_verified, Literal(ref_ms2_list[index])))
                            g.add((ref_ms_URI, glycan.ms1_verified, Literal(ref_ms1_list[index])))
                            g.add((ref_ms_URI, glycan.ms2_verified, Literal(ref_ms2_list[index])))


            #ce gu, c6
            if is_not_empty_array(row[5]):
                entry = row[5]
                # print "\nreading ce_gu cell....\n"

                add_gu_from_main(g,entry,glycobase_id,structure_URI,root_prefix.ce_peak,glycan.has_ce_peak,
                                 root_prefix.ce,glycan.evidence_ce,glycan.has_glucose_unit,"APTS")


            #rpuplc gu, c7
            if is_not_empty_array(row[6]):
                entry = row[6]
                # print "\nreading rpuplc_gu cell....\n"

                add_gu_from_main(g,entry,glycobase_id,structure_URI,root_prefix.lc_peak,glycan.has_lc_chromatogram_peak,
                                 root_prefix.rpuplc,glycan.evidence_rpuplc,glycan.has_arabinose_unit,"2-AB")


            #digestion_children, c11
            if is_not_empty_array(row[10]):
                print "\n reading digestion child cell...\n"

                process_digest(row[10],g,'child',glycobase_id)

            #digestion_parent, c12
            if is_not_empty_array(row[11]):
                print "\n reading digestion parent cell...\n"
                process_digest(row[11],g,'parent',glycobase_id)

            i+=1
        print '\nprocessed ' + str(i-1) + ' rows\n'
        stop = timeit.default_timer()
        time_s = stop-start
        time = math.modf(time_s/60)
        print '\n----------------------\nused ' + str(time[1]) + ' min ' + str(time[0]) +' s\n------------------------\n'

        # print g.serialize(format = 'turtle')
        return g.serialize(format = 'turtle')

# full_file = '/home/sophie/Dropbox/MQU/glycobase_spread.txt'
full_file = '/home/sophie/Dropbox/MQU/glycobase_spread2018.txt'
# macfile = '/Users/Sophie/Dropbox/MQU/1110/glycobase_spread.txt'

# tomac = '/Users/Sophie/Desktop/1111.ttl'
tofile = '/home/sophie/Dropbox/workfile/glycostore/glycobase180314.ttl'
f = open(tofile,'w')
f.write(csv2ttl(full_file))
f.close()
# print csv2ttl(full_file)





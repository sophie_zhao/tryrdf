from rdflib import URIRef, BNode, Literal
from rdflib.namespace import RDF, FOAF,OWL, DCTERMS, DC, XSD, RDFS
from rdflib import Graph
from rdflib import Namespace
import uuid

# ----------------  demo code -------------------
# bob = URIRef('http://example.org/people/Bob')
# linda = BNode()  # a GUID is generated
#
# name = Literal('Bob')  # passing a string
# age = Literal(24)  # passing a python int
# height = Literal(76.5)  # passing a python float
#
# g = Graph()
#
# g.add((bob, RDF.type, FOAF.Person))
# g.add((bob, FOAF.name, name))
# g.add((bob, FOAF.knows, linda))
# g.add((linda, RDF.type, FOAF.Person))
# g.add((linda, FOAF.name, Literal('Linda')))

# print g.serialize(format='turtle')



# -------------  object readin -----------------
# import csv
# with open('/home/sophie/Desktop/demo.csv', 'rb') as csvfile:
#     infile = csv.reader(csvfile, delimiter='\t', quotechar='|')
#     for row in infile:
#         uoxf = row[0]
#         glycobaseID = row[1]
#         ...


uoxf = Literal('A3G2')
glycobaseID = Literal('891')
ct_sequence = Literal('RES,1b:b-dglc-HEX-1:5,2s:n-acetyl,3b:b-dglc-HEX-1:5,4s:n-acetyl,5b:b-dman-HEX-1:5,6b:a-dman-HEX-1:5,7b:b-dglc-HEX-1:5,8s:n-acetyl,9b:b-dglc-HEX-1:5,10s:n-acetyl,11b:a-dman-HEX-1:5,12b:b-dglc-HEX-1:5,13s:n-acetyl,LIN,1:1d(2+1)2n,2:1o(4+1)3d,3:3d(2+1)4n,4:3o(4+1)5d,5:5o(3+1)6d,6:6o(2+1)7d,7:7d(2+1)8n,8:6o(4+1)9d,9:9d(2+1)10n,10:5o(6+1)11d,11:11o(2+1)12d,12:12d(2+1)13n,UND,UND1:100.0:100.0,ParentIDs:7|9|12,SubtreeLinkageID1:o(-1+1)d,RES,14b:b-dgal-HEX-1:5,UND2:100.0:100.0,ParentIDs:7|9|12,SubtreeLinkageID1:o(-1+1)d,RES,15b:b-dgal-HEX-1:5')
# uplc_gu = []
uplc_gu = Literal('')
# hplc_gu = [7.94, 7.46]
hplc_gu_1 = Literal('7.94')
hplc_gu_2 = Literal('7.46')
# ce_gu = []
ce_gu = Literal('')
# rpuplc_gu = []
rpuplc_gu = Literal('')
# taxonomy = [Homo sapiens neanderthalensis]
taxonomy = Literal('Homo sapiens')
# tissue = [Anatomy]
tissue = Literal('Anatomy')
# disease = []
disease = Literal('')
# report_title = [Published Glycans, Published Glycans]
report_title = Literal('Published Glycans')
# report_id = [1, 1]
report_id = Literal('1')
# sample_title = [Professor Rudd Other Data, Professor Rudd Other Data]
sample_title_1 = Literal('Professor Rudd Other Data')
sample_title_2 = Literal('Professor Rudd Other Data')
sample_id = Literal('69')


# ------------  set NameSpace for the 18 type data in glycobase  ---------------
# generate uuid for subject
uid_str_full = uuid.uuid4().urn
uid_str = uid_str_full[9:]

# Namespaces in glycan.owl that are missed in rdflib, put here in case of usage later
bibo = Namespace('http://purl.org/ontology/bibo/')
glycan = Namespace('http://purl.jp/bio/12/glyco/glycan#')
dcterms = Namespace('http://purl.org/dc/terms/')

# Namespaces for glycobase project
root_prefix = Namespace('http://rdf.glycobase.org/')
main_prefix = Namespace('http://rdf.glycobase.org/structure/')
main_URI = URIRef(main_prefix + glycobaseID)

glycobase_prefix = Namespace('https://glycobase.nibrt.ie/glycobase/show_glycan.action?glycanSequenceId=')
glycobase_URI = URIRef(glycobase_prefix + glycobaseID)
ct_sequence_URI = URIRef(main_prefix + glycobaseID + "/ct") # fit unicarbkb
evidence_lc_URI = URIRef(root_prefix + "evidence_lc/" + glycobaseID)
evidence_uplc_URI = URIRef(root_prefix + "evidence_lc/uplc/" + glycobaseID)
evidence_hplc_URI = URIRef(root_prefix + "evidence_lc/hplc/" + glycobaseID)
evidence_rpuplc_URI = URIRef(root_prefix + "evidence_lc/rpuplc/" + glycobaseID)
evidence_ce_URI = URIRef(main_prefix + "/evidence_ce/" + glycobaseID)
source_nature_URI = URIRef(main_prefix + glycobaseID + "/source_natural")
source_sample_URI = URIRef(main_prefix + glycobaseID + "/source_sample")
report_URI = URIRef(main_prefix + "reference/" + glycobaseID)

# subject type

# generate an example
g = Graph()
g.bind('glycan',glycan)
g.bind('bibo',bibo)
g.bind('dcterms',dcterms)
# g.bind('glycobase',main_prefix)
g.add((main_URI, RDF.type, glycan.saccharide))
g.add((main_URI, glycan.has_uoxf, uoxf))

g.add((main_URI, glycan.has_glycosequence,ct_sequence_URI))
g.add((ct_sequence_URI, RDF.type, glycan.glycosequence))
g.add((ct_sequence_URI, glycan.has_sequence, ct_sequence))
g.add((ct_sequence_URI, glycan.in_carbohydrate_format, glycan.carbohydrate_format_glycoct))

g.add((main_URI, glycan.in_glycan_database, glycobase_URI))
g.add((glycobase_URI, RDF.type, glycan.resource_entry))
g.add((glycobase_URI, glycan.in_glycan_database, glycan.database_glycobase))
g.add((glycobase_URI, DCTERMS.identifier,glycobaseID))

g.add((main_URI, glycan.evidence_lc, evidence_lc_URI))
g.add((evidence_lc_URI, glycan.uplc,evidence_uplc_URI))
g.add((evidence_uplc_URI, glycan.has_glucose_units, uplc_gu))
g.add((evidence_lc_URI, glycan.hplc, evidence_hplc_URI))
g.add((evidence_hplc_URI, glycan.has_glucose_units, hplc_gu_1))
g.add((evidence_hplc_URI, glycan.has_glucose_units, hplc_gu_2))
g.add((evidence_lc_URI, glycan.rpuplc, evidence_rpuplc_URI))
g.add((evidence_rpuplc_URI, glycan.has_glucose_units, rpuplc_gu))

g.add((main_URI, glycan.evidence_ce,evidence_ce_URI))
g.add((evidence_ce_URI, glycan.has_glucose_units, ce_gu))

g.add((main_URI, glycan.source, source_nature_URI))
g.add((main_URI, glycan.source, source_sample_URI))
g.add((source_nature_URI, RDF.type, glycan.source_nature))
g.add((source_nature_URI, glycan.has_taxon, taxonomy))
g.add((source_nature_URI, glycan.has_tissue, tissue))
g.add((source_nature_URI, glycan.has_disease, disease))
g.add((source_sample_URI, RDF.type, glycan.source_sample))
g.add((source_sample_URI, glycan.has_sample_type, sample_title_1))
g.add((source_sample_URI, glycan.has_sample_type, sample_title_2))
g.add((source_sample_URI, glycan.has_sample_id, sample_id))
g.add((main_URI, glycan.has_citation, report_URI))
g.add((report_URI, bibo.shortTitle, report_title))
print g.serialize(format = 'turtle')




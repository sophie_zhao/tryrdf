'''
The glytoucan file extracted from rdf endpoint contains 1. accession number 2. CT sequence 3.monoisotopic mass
This script try to match the CT for glycobase structures to glytoucan and extract corresponding accession number and monoisotopic mass
'''

import csv

def matchCT(ct):
    out=[]
    libfile="/home/sophie/Dropbox/workfile/glycostore/glytoucan.csv"
    with open(libfile) as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')
        for row in readCSV:
            libCT=row[1]
            if ct==libCT:
                acc=row[0]
                mass=row[2]
                out.append(acc)
                out.append(mass)
                return out
    return "not found"

def extractMass(id):
    masslib="/home/sophie/Dropbox/workfile/glycostore/glycostore_id_mass.csv"
    with open(masslib) as csvfile:
        next(csvfile)
        readCSV = csv.reader(csvfile, delimiter=',',quotechar='"')
        for row in readCSV:
            id_lib=row[0].replace('"','')
            if int(id)==int(id_lib):
                mass=row[1].replace('"','')
                return mass
        return "not found"

def readGlycoBase():
    libf="/home/sophie/Dropbox/MQU/glycobase_spread.txt"
    with open(libf) as csvfile:
        next(csvfile)
        readCSV = csv.reader(csvfile, delimiter='\t')
        for row in readCSV:
            # ct= row[2].replace(",","\n")
            # ct=ct.replace("1:u(-1+1)u","1:o(-1+1)d")
            # glytoucan=matchCT(ct)
            # if glytoucan=='not found':
            #     print row[0], row[1], " is not in glytoucan"
            # else:
            #     print row[0],row[1],glytoucan
            id=row[1]
            mass=extractMass(id)
            if mass=='not found':
                print id, " not found"
            else:
                print mass # str type

#
# res="RES\n1b:x-dglc-HEX-1:5\n2s:n-acetyl\n3b:b-dglc-HEX-1:5\n4s:n-acetyl\n5b:b-dman-HEX-1:5\n6b:a-dman-HEX-1:5\n7b:b-dglc-HEX-1:5\n8s:n-acetyl\n9b:b-dgal-HEX-1:5\n10b:a-dgal-HEX-1:5\n11b:b-dglc-HEX-1:5\n12s:n-acetyl\n13b:a-dman-HEX-1:5\n14b:b-dglc-HEX-1:5\n15s:n-acetyl\n16b:b-dgal-HEX-1:5\n17b:a-dgro-dgal-NON-2:6|1:a|2:keto|3:d\n18s:n-glycolyl\n19b:a-dgro-dgal-NON-2:6|1:a|2:keto|3:d\n20s:n-glycolyl\nLIN\n1:1d(2+1)2n\n2:1o(4+1)3d\n3:3d(2+1)4n\n4:3o(4+1)5d\n5:5o(3+1)6d\n6:6o(2+1)7d\n7:7d(2+1)8n\n8:7o(4+1)9d\n9:9o(3+1)10d\n10:5o(4+1)11d\n11:11d(2+1)12n\n12:5o(6+1)13d\n13:13o(2+1)14d\n14:14d(2+1)15n\n15:14o(4+1)16d\n16:16o(3|6+2)17d\n17:17d(5+1)18n\n18:17o(8+2)19d\n19:19d(5+1)20n"
# print matchCT(res)
# # readGlycoBase()
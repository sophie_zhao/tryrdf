import re


# for full list of monosaccharide http://www.genome.jp/kegg/catalog/codes2.html
Free_reducing_end = 18.01056
Reduced_reducing_end = 20.02621
Hex = 162.05282      # man, gal, glc --> G = Gal
HexNAc = 203.07937   # galnac, glcnac  --> A = GlcNAc
Deoxyhex = 146.05791 # fuc --> F = Fuc
NeuAc = 291.09542  # S
NeuGc = 307.09033  # Sg
Pentose = 132.04226
# Sulphate = 79.95682
# Phosphate = 79.96633
# HexA = 176.03209
monosaccharide_dict={'F':146.05791,'A':203.07937,'B':203.07937,'G':162.05282,'M':162.05282,'S':291.09542,'Sg':307.09033,'GlcNAc':203.07937,'Lac':365.13219}


def mass_by_uoxf(uoxf):
    # print uoxf
    no_iso = uoxf.split(" ")[0]
    no_bracket = re.sub("[\(\[].*?[\)\]]", "", no_iso)
    mono_list = re.split('(\d+)',no_bracket)   # split by number
    num_item = len(mono_list)-1  # last item is empty

    core_mass = Hex*3 + HexNAc*2
    mass = Free_reducing_end
    comp_dict = {}
    # print mono_list
    for i in range(num_item): # count monosaccharide
        if not mono_list[i].isdigit() and len(mono_list[i])>1: # possible combinations: Sg, Lac, GlcNAc, Ga (alpha-gal)
            if 'Sg' in mono_list[i]:
                if 'Sg' in comp_dict:
                    comp_dict['Sg'] += int(mono_list[i+1])
                else:
                    comp_dict['Sg'] = int(mono_list[i+1])
            elif 'Lac' in mono_list[i]:
                if 'Lac' in comp_dict:
                    comp_dict['Lac'] += int(mono_list[i+1]) # there is always a number follow Lac in waters data
                else:
                    comp_dict['Lac'] = int(mono_list[i+1])
            elif 'GlcNAc' in mono_list[i]:
                if 'GlcNAc' in comp_dict:
                    comp_dict['GlcNAc'] += int(mono_list[i+1])
                else:
                    comp_dict['GlcNAc'] = int(mono_list[i+1])
            elif 'Ga' in mono_list[i]:
                if 'G' in comp_dict:
                    comp_dict['G'] += int(mono_list[i+1])
                else:
                    comp_dict['G'] = int(mono_list[i+1])
            else: # assume at most two monosaccharide together
                # print mono_list[i]
                if mono_list[i][0] in comp_dict:
                    comp_dict[mono_list[i][0]] += 1
                else:
                    comp_dict[mono_list[i][0]] = 1
                if mono_list[i][1] in comp_dict:
                    comp_dict[mono_list[i][1]] += int(mono_list[i+1])
                else:
                    comp_dict[mono_list[i][1]] = int(mono_list[i+1])
        else:  # single letter residue
            if not mono_list[i].isdigit():
                if mono_list[i] in comp_dict:
                    comp_dict[mono_list[i]] += int(mono_list[i+1])
                else:
                    comp_dict[mono_list[i]] = int(mono_list[i+1])
    if 'A' in comp_dict:
        if 'M' in comp_dict:
            mass += HexNAc*2
        else:
            mass += core_mass
    elif 'M' in comp_dict:
        mass += HexNAc*2
    # print comp_dict
    # print mass
    for key, value in comp_dict.items():
        mass += monosaccharide_dict[key]*value
    # print mass

    return round(mass,4)




#
# t1="A3S(6)1G(4,4,3)3S(3,3)2 iso2"
# t2="M9 a3D3,a2D4(2)"
# t3="F(6)A2G(4)2Sg(3,6)2"
# t4="F(6)A4G(4)4Lac1S3" #Lac = GlcNAc + Gal = hexnac + hex
# t5='M5A1G(4)1S1'
# t6='A4G(4)1GlcNAc1'
# t7='F(6)A2[6]G(4)1S(6)1'
# t8='M3'
# t9='F(6)A2[3]G(4)1'
# t10='M16 a3D1,[D2(6),D3(6)],a2D4(2)'
#
# print mass_by_uoxf(t10)
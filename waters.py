from sets import Set
from rdflib import URIRef, Literal
from rdflib.namespace import RDF, DCTERMS, XSD
from rdflib import Graph
from prefix import glycan, root_prefix, bibo,dc,dcterms,owl
from mass_by_uoxf import mass_by_uoxf
import sys
import csv
import timeit
start = timeit.default_timer()
maxInt = sys.maxsize
decrement = True

while decrement:
    # decrease the maxInt value by factor 10
    # as long as the OverflowError occurs.
    decrement = False
    try:
        csv.field_size_limit(maxInt)
    except OverflowError:
        maxInt = int(maxInt/10)
        decrement = True

# if the String inside [ ] is not empty, return True
def is_not_empty_array(str):
    if str[1:len(str)-1]:
        return True
    else:
        return False

# extract content inside [ ], make them into a list of strings
def str_to_list(str):
    content = str[1:len(str)-1]
    list_of_str = content.split(",")
    return list_of_str

def remove_last_comma(str):
    new_str = str[0:len(str)-1]
    return new_str

def add_gu_from_main(g,cell_content,glycobase_id,structure_URI,peak_predicate,has_predicate,
                                 evidence_predicate, evidence_type,guau_predicate,label):
    content = cell_content[2:len(cell_content)-2]
    evidence_list = content.split("], [Glucose")
    for item in evidence_list:
        list = item.split(":")

        gu = list[1].split(",")[0]
        if float(gu) != 0:
            report = list[2].split(",")[0]
            report_id = list[3].split(",")[0] # here use report id as profile
            sample = list[4].split(",")[0]
            sample_id = list[5].split(",")[0]
            # # print gu + '\t' + area + '\t'+ number +'\t' + report + '\t'+report_id + '\t' + sample + '\t'+ sample_id + '\t' + profile_id

            ref_comp_URI = URIRef(root_prefix.referencecompound + '/' + glycobase_id + '-' + report_id)
            g.add((ref_comp_URI, RDF.type, glycan.referenced_compound))
            g.add((ref_comp_URI, glycan.has_glycan, structure_URI))
            g.add((ref_comp_URI, glycan.is_from_report, Literal(report)))

            protein_URI = URIRef(root_prefix.proteinsummary + '/'+ sample_id)
            if sample in ('Erythropoietin', 'Enbrel', 'Human IgG', 'Herceptin', 'EPO', 'Mouse IgG', 'Infliximab', 'Invertase', 'Fetuin', 'NIST'):
                if 'EPO' in sample:
                    sample = 'Erythropoietin'
                g.add((protein_URI, glycan.has_protein_name, Literal(sample)))
                g.add((protein_URI, glycan.has_attached_glycan, structure_URI))

            source_URI = URIRef(root_prefix.source + '/' + sample_id)
            g.add((ref_comp_URI, glycan.is_from_source, source_URI))
            g.add((source_URI, RDF.type, glycan.source_nature))
            g.add((source_URI, glycan.from_sample, Literal('Waters-'+sample)))

            # peak_predicate refers to: root_prefix.lc_peak
            peak_URI = URIRef(peak_predicate + '/' + glycobase_id+'-'+report_id) # where gu is stored
            # has_predicate refers to: glycan.has_lc_chromatogram_peak
            g.add((ref_comp_URI, has_predicate, peak_URI))
            g.add((structure_URI, has_predicate, peak_URI))
            # evidence_predicate refers to: root_prefix.hplc; root_prefix.uplc
            evidence_profile_URI = URIRef(evidence_predicate + '/' + report_id)
            g.add((ref_comp_URI,glycan.is_from_profile, evidence_profile_URI))
            # evidence_type refers to: glycan.evidence_rpuplc; glycan.evidence_uplc; glycan.evidence_hplc; glycan.evidence_ce
            g.add((evidence_profile_URI, RDF.type, evidence_type))
            g.add((evidence_profile_URI, has_predicate, peak_URI)) # has_predicate is a variable depend on situations
            g.add((evidence_profile_URI, glycan.has_label, Literal(label)))
            g.add((evidence_profile_URI, glycan.contributed_by, Literal("WATERS")))
            # guau_predicate refers to: glycan.has_arabinose_unit or glycan.has_glucose_unit
            g.add((peak_URI, guau_predicate, Literal(gu,datatype=XSD.float)))
    return g

def read_evidence_cell(entry):
    content = entry[2:len(entry)-2]
    evidence_list = content.split("], [Glucose")
    output_set = Set([])
    for item in evidence_list:
        list = item.split(":")

        gu = list[1].split(",")[0]
        if float(gu) != 0:
            report = list[2].split(",")[0]
            report_id = list[3].split(",")[0]
            sample = list[4].split(",")[0]
            sample_id = list[5].split(",")[0]
            output_set.add(sample)
    return output_set

def read_waters(csvfile):
    g = Graph()
    g.bind('glycan',glycan)
    g.bind('bibo',bibo)
    g.bind('dcterms',dcterms)
    g.bind('dc',dc)
    g.bind('owl', owl)

    with open(csvfile, 'rb') as csvfile:
        next(csvfile) # skip the header in the first row
        infile = csv.reader(csvfile, delimiter='\t', quotechar='|')
        i = 1
        sample_id_set=Set([])
        for row in infile:
            # print '\n start ' + str(i) + " row\n"
            uoxf = row[0]
            # print uoxf
            glycobase_id = row[1]
            # ct_sequence = row[2]
            evidence_2ab = row[3]
            evidence_2aa = row[4]
            evidence_rfms = row[5]

            structure_URI = URIRef(root_prefix.structure + '/' + glycobase_id)
            ct_sequence_URI = URIRef(root_prefix.structure + '/' + glycobase_id + "/ct")

            # start a statement for structure class
            g.add((structure_URI, RDF.type, glycan.saccharide))
            g.add((structure_URI, glycan.glycosequence, ct_sequence_URI))
            g.add((structure_URI, glycan.has_glycosequence, ct_sequence_URI))
            g.add((structure_URI, glycan.has_uoxf, Literal(uoxf)))
            g.add((structure_URI, glycan.has_glycobase_id,Literal(glycobase_id,datatype=XSD.integer)))

            # add glycoct sequence
            ct_content = row[2].replace(",","\n")
            ct_sequence = Literal(ct_content)
            g.add((ct_sequence_URI, RDF.type, glycan.glycosequence))
            g.add((ct_sequence_URI, glycan.has_sequence, ct_sequence))
            g.add((ct_sequence_URI, glycan.in_carbohydrate_format, glycan.carbohydrate_format_glycoct))

            # add mass
            mass = mass_by_uoxf(uoxf)
            g.add((structure_URI, glycan.has_monoisotopic_molecular_weight, Literal(mass,datatype=XSD.float)))

            if is_not_empty_array(evidence_2ab):
                entry = evidence_2ab
                # print "\nreading uplc_gu cell....\n"

                add_gu_from_main(g,entry,glycobase_id,structure_URI,root_prefix.lc_peak,glycan.has_lc_chromatogram_peak,
                                 root_prefix.uplc,glycan.evidence_uplc,glycan.has_glucose_unit,"2-AB")

            if is_not_empty_array(evidence_2aa):
                entry = evidence_2aa
                # print "\nreading uplc_gu cell....\n"

                add_gu_from_main(g,entry,glycobase_id,structure_URI,root_prefix.lc_peak,glycan.has_lc_chromatogram_peak,
                                 root_prefix.uplc,glycan.evidence_uplc,glycan.has_glucose_unit,"2-AA")

            if is_not_empty_array(evidence_rfms):
                entry = evidence_rfms
                # print "\nreading uplc_gu cell....\n"

                add_gu_from_main(g,entry,glycobase_id,structure_URI,root_prefix.lc_peak,glycan.has_lc_chromatogram_peak,
                                 root_prefix.uplc,glycan.evidence_uplc,glycan.has_glucose_unit,"RFMS")

            i+=1
            # if i == 3:
            #     break

        print '\nprocessed ' + str(i) + ' rows\n'
        return g.serialize(format = 'turtle')

full_file = '/home/sophie/Dropbox/workfile/glycostore/WATERS_data_in_use.txt'
tofile = '/home/sophie/Dropbox/workfile/glycostore/waters180508.ttl'

f = open(tofile,'w')
f.write(read_waters(full_file))
f.close()

# read_waters()




import sys
import csv
maxInt = sys.maxsize
decrement = True

while decrement:
    # decrease the maxInt value by factor 10
    # as long as the OverflowError occurs.
    decrement = False
    try:
        csv.field_size_limit(maxInt)
    except OverflowError:
        maxInt = int(maxInt/10)
        decrement = True

def split_literature(str):
    list = str[2:len(str)-2] #remove 2 from the back because the literature entry ends with ']]'
    list_of_reference = list.split("], [")
    return list_of_reference

def remove_last_comma(str):
    str_no_rspace = str.strip()
    new_str = str_no_rspace[0:len(str_no_rspace)-1]
    return new_str

def extract_literature_element(item):
    cformat = item.replace("reference_id:","|").replace("author: ","|").replace("paper_gu: ","|").\
                            replace("paper_title: ","|").replace("paper_journal: ","|").replace("paper_issue: ","|").\
                            replace("paper_pages: ","|").replace("paper_date: ","|").replace("paper_pubmed: ","|").\
                            replace("MS/MS: ","|").replace("MS: ","|")
    list = cformat.split("|")
    return list
#-----------------------------------remove above code after testing literature column ------------------------------------


def is_not_empty_array(str):
    if str[1:len(str)-1]:
        return True
    else:
        return False

def digest_list(str):
    list = str[2:len(str)-3] # remove 3 from the back because the digestion entry end with '],]'
    list_of_digest = list.split("],[")
    return list_of_digest

# extract content inside [ ], make them into a list of strings
def str_to_list(str):
    content = str[1:len(str)-1]
    list_of_str = content.split(",")
    return list_of_str

# tofile = '/home/sophie/Desktop/parent_enzyme.txt'
# f = open(tofile,'w')

import csv
tofile = '/home/sophie/Desktop/literature1110.txt'
file = open(tofile,'w')

# with open('/home/sophie/Downloads/glycobase_spread.csv', 'rb') as csvfile:
#     next(csvfile)
with open('/home/sophie/Desktop/glycobase_spread.txt') as csvfile:
    next(csvfile)
    infile = csv.reader(csvfile, delimiter='\t', quotechar='|')

    for row in infile:
        root_uoxf = row[0]
    #     root_glycobaseid = row[1]
    #
    #     if is_not_empty_array(row[3]):
    #         uplc_gu = str_to_list(row[3])
    #         uplc = len(uplc_gu)
    #     else:
    #         uplc = 0
    #     if is_not_empty_array(row[4]):
    #         hplc_gu = str_to_list(row[4])
    #         hplc = len(hplc_gu)
    #     else:
    #         hplc = 0
    #     if is_not_empty_array(row[5]):
    #         ce_gu = str_to_list(row[5])
    #         ce = len(ce_gu)
    #     else:
    #         ce = 0
    #     if is_not_empty_array(row[6]):
    #         rpuplc_au = str_to_list(row[6])
    #         rpuplc = len(rpuplc_au)
    #     else:
    #         rpuplc = 0
    #     if not uplc == hplc == ce == rpuplc ==0:
    #         file.write( root_glycobaseid + '\t'+root_uoxf+'\t'+str(uplc)+'\t'+str(hplc) + '\t'+str(ce)+'\t'+str(rpuplc)+'\n')

        if is_not_empty_array(row[12]):
            list_of_reference = split_literature(row[12]) # should be [reference,reference,...]
        #    reference = len(list_of_reference)
        # else:
        #     reference = 0
        # f.write(str(uplc)+'\t'+str(hplc)+'\t'+str(ce)+'\t'+str(reference)+'\n')
            for item in list_of_reference: # item is a literature
                # print item # list every reference
                array_in_item = extract_literature_element(item)
                length = len(array_in_item)
                if length == 11:
                    referece_id = remove_last_comma(array_in_item[0].strip())
                    pubmed_id = remove_last_comma(array_in_item[8])
                    gu = remove_last_comma(array_in_item[2])
                    ms2 = remove_last_comma(array_in_item[9])
                    ms1 = array_in_item[10]
                else: # length = 12
                    referece_id = remove_last_comma(array_in_item[1].strip())
                    pubmed_id = remove_last_comma(array_in_item[9])
                    gu = remove_last_comma(array_in_item[3])
                    ms2 = remove_last_comma(array_in_item[10])
                    ms1 = array_in_item[11]
                file.write(pubmed_id+'\n')
                # file.write(root_uoxf + '\t'+referece_id+'\t'+pubmed_id+'\t'+gu+'\t'+ms2+'\t'+ms1+'\n')

file.close()


        # i = 0
        # while i < 18:
        #     if i < 3:
        #         # direct read in the row[i] to statement object using literal()
        #         print row[i]
        #     elif i<16 and str2list(row[i]):
        #         # if not empty list
        #         content = str2list(row[i])
        #         # convert to a list of string
        #         list_of_str = content.split(",")
        #         print str2list(row[i])
        #         for item in list_of_str:
        #             print "read separetly " + item
        #     elif i==16:
        #         for item in digest_list(row[i]):
        #             print "digest child: " + item
        #             array_in_item = item.split(",")
        #             j = 0
        #             while j<9:
        #                 print array_in_item[j]
        #                 j += 1
        #
        #
        #     elif i == 17:
        #         for item in digest_list(row[i]):
        #             print "digest parent: " + item
        #             cformat = item.replace("Technique:","|").replace("name:","|").replace("enzymes:","|").\
        #                 replace("retention (GU): ","|").replace("profile ID: ","|").replace("profile name:","|").\
        #                 replace("profile instrument: ","|").replace("profile dextran standard: ","|")
        #             array_in_item = cformat.split("|")
        #             print cformat
        #             j = 0
        #             while j<9:
        #                 print array_in_item[j][0:len(array_in_item[j])-1]
        #                 j += 1
        #
        #
        #     else:
        #         # empty value array, can just skip read into RDF structure
        #         print "content not exist"
        #     i= i+1
        # #


